const express = require('express');
const notesCntrl = require('../controllers/notesCntrl');

const router = express.Router();
const testRouter = express.Router();

router.use('/notes', testRouter);

testRouter.get('/', notesCntrl.getAllNotes);
testRouter.get('/:slug', notesCntrl.getNote);
testRouter.get('/favorites/all', notesCntrl.getFavorites);
testRouter.post('/:id/toggleFavorites', notesCntrl.toggleFavorites);
testRouter.post('/find', notesCntrl.findNote);
testRouter.post('/', notesCntrl.addOrEditNote);
testRouter.post('/:id', notesCntrl.addOrEditNote);
testRouter.delete('/:id', notesCntrl.deleteNote);

module.exports = router;
