const Notes = require('../models/notes');
const { ApiResponseSuccess, ApiResponseFail } = require('../utils/api-response');

exports.getAllNotes = (req, res, next) => {
  Notes.find()
    .then((notes) => {
      if (!notes) {
        throw new ApiResponseFail('There is no Notes found', 404);
      }
      new ApiResponseSuccess(notes).send(res);
    })
    .catch(next);
};

exports.getNote = (req, res, next) => {
  const slug = req.params.slug;
  Notes.findOne({ slug })
    .then((note) => {
      if (!note || note.length < 1) {
        throw new ApiResponseFail('There is no note with same slug', 404);
      }
      new ApiResponseSuccess(note).send(res);
    })
    .catch(next);
};

exports.findNote = (req, res, next) => {
  const { title, cat, tags } = req.body;
  const query = {};
  if (!title) {
    throw new ApiResponseFail('You must enter title of the note for search', 422);
  } else {
    query.title = new RegExp(title, 'i');
  }
  if (cat) {
    query.cat = cat;
  }
  if (tags) {
    query.tags = tags;
  }
  Notes.find(query)
    .then(data => new ApiResponseSuccess(data).send(res))
    .catch(next);
}

exports.getFavorites = (req, res, next) => {
  Notes.find({ isFavorite: true })
    .then((notes) => {
      new ApiResponseSuccess(notes).send(res);
    })
    .catch(next);
}

exports.toggleFavorites = (req, res, next) => {
  const id = req.params.id;
  Notes.findById(id)
    .then((note) => {
      note.toggleFavorite()
        .then(data => new ApiResponseSuccess(data).send(res));
    })
    .catch(next);
}

exports.addOrEditNote = (req, res, next) => {
  const id = req.params.id;
  const { title, content } = req.body;
  if (!title || !content) {
    throw new ApiResponseFail('You must enter title and content of note.', 422);
  }
  const getNote = () => {
    if (!id) return Promise.resolve(new Notes());
    return Notes.findById(id);
  };
  getNote()
    .then((note) => {
      if (!note) {
        throw new ApiResponseFail('Page not found.', 404);
      }
      note.set(Object.assign({}, req.body));
      return note.save();
    })
    .then(note => new ApiResponseSuccess(note).send(res))
    .catch(next);
};

exports.deleteNote = (req, res, next) => {
  const id = req.params.id;
  Notes.findById(id).remove()
    .then(() => {
      new ApiResponseSuccess({}).send(res);
    })
    .catch(next);
};

