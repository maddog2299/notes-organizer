const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SubtasksSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  start_date: {
    type: Date,
    default: Date.now,
    required: true
  },
  end_date: {
    type: Date
  },
  status: {
    type: String,
    default: 'In Progress'
  }
});

module.exports = SubtasksSchema;
