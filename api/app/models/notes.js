const mongoose = require('mongoose');
const SubtasksSchema = require('./subtasks');

const Schema = mongoose.Schema;

const NotesSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  tags: {
    type: Array
  },
  task_type: {
    type: String,
    default: 'ASAP'
  },
  cat: {
    type: String,
    default: 'No Category',
    required: true
  },
  start_date: {
    type: Date,
    default: Date.now,
    required: true
  },
  end_date: {
    type: Date,
  },
  status: {
    type: String,
    default: 'New'
  },
  image: {
    type: String
  },
  slug: {
    type: String,
    required: true,
    unique: true
  },
  priotity: {
    type: String,
    default: 'Low'
  },
  isFavorite: {
    type: Boolean,
    default: false
  },
  sub_tasks: [SubtasksSchema]
});

NotesSchema.methods.toggleFavorite = function toggleFavorite() {
  this.isFavorite ? this.set({ isFavorite: false }) : this.set({ isFavorite: true });
  return this.save();
}

module.exports = mongoose.model('Notes', NotesSchema);
