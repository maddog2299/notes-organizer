const mongoose = require('mongoose');
const config = require('config');
const Promise = require('bluebird');

module.exports = () => {
    // Use native promises
  mongoose.Promise = Promise;
    // Connect to database
  mongoose.connect(config.get('database'));
};
