const Promise = require('bluebird');
const winston = require('./winston');
const database = require('./database');
const middleware = require('./middleware');
const errorHandling = require('./error-handling');

module.exports = app =>
  Promise
    .each([
      winston,
      database,
      middleware,
      errorHandling
    ], func => func(app))
    .then(() => app);

