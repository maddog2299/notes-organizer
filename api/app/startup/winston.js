const winston = require('winston');

module.exports = () => {
  winston.loggers.add('backend', {
    console: {
      level: 'silly',
      colorize: true,
      timestamp: true,
      label: 'Backend'
    },
    file: {
      filename: 'storage/logs/backend.log'
    }
  });
};
