const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('config');
const morgan = require('morgan');
const routes = require('../routes');
const apiResponseMiddleware = require('../middleware/api-response');


module.exports = (app) => {
  app.set('view engine', 'pug');
  app.use('/assets', express.static('public'));
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  if (config.util.getEnv('NODE_ENV') !== 'test') {
    app.use(morgan('dev'));
  }
  app.use(routes);
  app.use(apiResponseMiddleware);
};
