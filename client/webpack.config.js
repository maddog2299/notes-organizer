function buildConfig(env) {
  return require(`./configs/webpack/${env}.js`)({ env });
}

module.exports = buildConfig;
