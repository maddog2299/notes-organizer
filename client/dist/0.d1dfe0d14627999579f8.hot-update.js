webpackHotUpdate(0,{

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _Drawer = __webpack_require__(225);

var _Drawer2 = _interopRequireDefault(_Drawer);

var _MenuItem = __webpack_require__(102);

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _viewList = __webpack_require__(658);

var _viewList2 = _interopRequireDefault(_viewList);

var _noteAdd = __webpack_require__(656);

var _noteAdd2 = _interopRequireDefault(_noteAdd);

var _search = __webpack_require__(657);

var _search2 = _interopRequireDefault(_search);

var _favorite = __webpack_require__(655);

var _favorite2 = _interopRequireDefault(_favorite);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AsideMenuComponent = function (_React$Component) {
	_inherits(AsideMenuComponent, _React$Component);

	function AsideMenuComponent() {
		_classCallCheck(this, AsideMenuComponent);

		return _possibleConstructorReturn(this, (AsideMenuComponent.__proto__ || Object.getPrototypeOf(AsideMenuComponent)).apply(this, arguments));
	}

	_createClass(AsideMenuComponent, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'aside-wrapper' },
				_react2.default.createElement(
					_Drawer2.default,
					{ containerClassName: 'aside-wrapper--overlay', open: true, width: 75 },
					_react2.default.createElement(
						_MenuItem2.default,
						null,
						_react2.default.createElement(_viewList2.default, null)
					),
					_react2.default.createElement(
						_MenuItem2.default,
						null,
						_react2.default.createElement(_noteAdd2.default, null)
					),
					_react2.default.createElement(
						_MenuItem2.default,
						null,
						_react2.default.createElement(_search2.default, null)
					),
					_react2.default.createElement(
						_MenuItem2.default,
						null,
						_react2.default.createElement(_favorite2.default, null)
					)
				)
			);
		}
	}]);

	return AsideMenuComponent;
}(_react2.default.Component);

exports.default = AsideMenuComponent;

/***/ }),

/***/ 775:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(796);
__webpack_require__(326);
__webpack_require__(797);
module.exports = __webpack_require__(323);


/***/ }),

/***/ 795:
false,

/***/ 796:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/* global __resourceQuery */
var url = __webpack_require__(320);
var stripAnsi = __webpack_require__(313);
var socket = __webpack_require__(322);
var overlay = __webpack_require__(321);

function getCurrentScriptSource() {
	// `document.currentScript` is the most accurate way to find the current script,
	// but is not supported in all browsers.
	if(document.currentScript)
		return document.currentScript.getAttribute("src");
	// Fall back to getting all scripts in the document.
	var scriptElements = document.scripts || [];
	var currentScript = scriptElements[scriptElements.length - 1];
	if(currentScript)
		return currentScript.getAttribute("src");
	// Fail as there was no script to use.
	throw new Error("[WDS] Failed to get current script source");
}

var urlParts;
if(true) {
	// If this bundle is inlined, use the resource query to get the correct url.
	urlParts = url.parse(__resourceQuery.substr(1));
} else {
	// Else, get the url from the <script> this file was called with.
	var scriptHost = getCurrentScriptSource();
	scriptHost = scriptHost.replace(/\/[^\/]+$/, "");
	urlParts = url.parse((scriptHost ? scriptHost : "/"), false, true);
}

var hot = false;
var initial = true;
var currentHash = "";
var logLevel = "info";
var useWarningOverlay = false;
var useErrorOverlay = false;

function log(level, msg) {
	if(logLevel === "info" && level === "info")
		return console.log(msg);
	if(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning")
		return console.warn(msg);
	if(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error")
		return console.error(msg);
}

// Send messages to the outside, so plugins can consume it.
function sendMsg(type, data) {
	if(typeof self !== "undefined" && self.window) {
		self.postMessage({
			type: "webpack" + type,
			data: data
		}, "*");
	}
}

var onSocketMsg = {
	hot: function() {
		hot = true;
		log("info", "[WDS] Hot Module Replacement enabled.");
	},
	invalid: function() {
		log("info", "[WDS] App updated. Recompiling...");
		sendMsg("Invalid");
	},
	hash: function(hash) {
		currentHash = hash;
	},
	"still-ok": function() {
		log("info", "[WDS] Nothing changed.")
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		sendMsg("StillOk");
	},
	"log-level": function(level) {
		logLevel = level;
	},
	"overlay": function(overlay) {
		if(typeof document !== "undefined") {
			if(typeof(overlay) === "boolean") {
				useWarningOverlay = overlay;
				useErrorOverlay = overlay;
			} else if(overlay) {
				useWarningOverlay = overlay.warnings;
				useErrorOverlay = overlay.errors;
			}
		}
	},
	ok: function() {
		sendMsg("Ok");
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		if(initial) return initial = false;
		reloadApp();
	},
	"content-changed": function() {
		log("info", "[WDS] Content base changed. Reloading...")
		self.location.reload();
	},
	warnings: function(warnings) {
		log("info", "[WDS] Warnings while compiling.");
		var strippedWarnings = warnings.map(function(warning) {
			return stripAnsi(warning);
		});
		sendMsg("Warnings", strippedWarnings);
		for(var i = 0; i < strippedWarnings.length; i++)
			console.warn(strippedWarnings[i]);
		if(useWarningOverlay) overlay.showMessage(warnings);

		if(initial) return initial = false;
		reloadApp();
	},
	errors: function(errors) {
		log("info", "[WDS] Errors while compiling. Reload prevented.");
		var strippedErrors = errors.map(function(error) {
			return stripAnsi(error);
		});
		sendMsg("Errors", strippedErrors);
		for(var i = 0; i < strippedErrors.length; i++)
			console.error(strippedErrors[i]);
		if(useErrorOverlay) overlay.showMessage(errors);
	},
	error: function(error) {
		console.error(error);
	},
	close: function() {
		log("error", "[WDS] Disconnected!");
		sendMsg("Close");
	}
};

var hostname = urlParts.hostname;
var protocol = urlParts.protocol;


//check ipv4 and ipv6 `all hostname`
if(hostname === "0.0.0.0" || hostname === "::") {
	// why do we need this check?
	// hostname n/a for file protocol (example, when using electron, ionic)
	// see: https://github.com/webpack/webpack-dev-server/pull/384
	if(self.location.hostname && !!~self.location.protocol.indexOf("http")) {
		hostname = self.location.hostname;
	}
}

// `hostname` can be empty when the script path is relative. In that case, specifying
// a protocol would result in an invalid URL.
// When https is used in the app, secure websockets are always necessary
// because the browser doesn't accept non-secure websockets.
if(hostname && (self.location.protocol === "https:" || urlParts.hostname === "0.0.0.0")) {
	protocol = self.location.protocol;
}

var socketUrl = url.format({
	protocol: protocol,
	auth: urlParts.auth,
	hostname: hostname,
	port: (urlParts.port === "0") ? self.location.port : urlParts.port,
	pathname: urlParts.path == null || urlParts.path === "/" ? "/sockjs-node" : urlParts.path
});

socket(socketUrl, onSocketMsg);

var isUnloading = false;
self.addEventListener("beforeunload", function() {
	isUnloading = true;
});

function reloadApp() {
	if(isUnloading) {
		return;
	}
	if(hot) {
		log("info", "[WDS] App hot update...");
		var hotEmitter = __webpack_require__(185);
		hotEmitter.emit("webpackHotUpdate", currentHash);
		if(typeof self !== "undefined" && self.window) {
			// broadcast update to window
			self.postMessage("webpackHotUpdate" + currentHash, "*");
		}
	} else {
		log("info", "[WDS] App updated. Reloading...");
		self.location.reload();
	}
}

/* WEBPACK VAR INJECTION */}.call(exports, "?http://localhost:8081"))

/***/ }),

/***/ 797:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/* global __resourceQuery */
var url = __webpack_require__(320);
var stripAnsi = __webpack_require__(313);
var socket = __webpack_require__(322);
var overlay = __webpack_require__(321);

function getCurrentScriptSource() {
	// `document.currentScript` is the most accurate way to find the current script,
	// but is not supported in all browsers.
	if(document.currentScript)
		return document.currentScript.getAttribute("src");
	// Fall back to getting all scripts in the document.
	var scriptElements = document.scripts || [];
	var currentScript = scriptElements[scriptElements.length - 1];
	if(currentScript)
		return currentScript.getAttribute("src");
	// Fail as there was no script to use.
	throw new Error("[WDS] Failed to get current script source");
}

var urlParts;
if(true) {
	// If this bundle is inlined, use the resource query to get the correct url.
	urlParts = url.parse(__resourceQuery.substr(1));
} else {
	// Else, get the url from the <script> this file was called with.
	var scriptHost = getCurrentScriptSource();
	scriptHost = scriptHost.replace(/\/[^\/]+$/, "");
	urlParts = url.parse((scriptHost ? scriptHost : "/"), false, true);
}

var hot = false;
var initial = true;
var currentHash = "";
var logLevel = "info";
var useWarningOverlay = false;
var useErrorOverlay = false;

function log(level, msg) {
	if(logLevel === "info" && level === "info")
		return console.log(msg);
	if(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning")
		return console.warn(msg);
	if(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error")
		return console.error(msg);
}

// Send messages to the outside, so plugins can consume it.
function sendMsg(type, data) {
	if(typeof self !== "undefined" && self.window) {
		self.postMessage({
			type: "webpack" + type,
			data: data
		}, "*");
	}
}

var onSocketMsg = {
	hot: function() {
		hot = true;
		log("info", "[WDS] Hot Module Replacement enabled.");
	},
	invalid: function() {
		log("info", "[WDS] App updated. Recompiling...");
		sendMsg("Invalid");
	},
	hash: function(hash) {
		currentHash = hash;
	},
	"still-ok": function() {
		log("info", "[WDS] Nothing changed.")
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		sendMsg("StillOk");
	},
	"log-level": function(level) {
		logLevel = level;
	},
	"overlay": function(overlay) {
		if(typeof document !== "undefined") {
			if(typeof(overlay) === "boolean") {
				useWarningOverlay = overlay;
				useErrorOverlay = overlay;
			} else if(overlay) {
				useWarningOverlay = overlay.warnings;
				useErrorOverlay = overlay.errors;
			}
		}
	},
	ok: function() {
		sendMsg("Ok");
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		if(initial) return initial = false;
		reloadApp();
	},
	"content-changed": function() {
		log("info", "[WDS] Content base changed. Reloading...")
		self.location.reload();
	},
	warnings: function(warnings) {
		log("info", "[WDS] Warnings while compiling.");
		var strippedWarnings = warnings.map(function(warning) {
			return stripAnsi(warning);
		});
		sendMsg("Warnings", strippedWarnings);
		for(var i = 0; i < strippedWarnings.length; i++)
			console.warn(strippedWarnings[i]);
		if(useWarningOverlay) overlay.showMessage(warnings);

		if(initial) return initial = false;
		reloadApp();
	},
	errors: function(errors) {
		log("info", "[WDS] Errors while compiling. Reload prevented.");
		var strippedErrors = errors.map(function(error) {
			return stripAnsi(error);
		});
		sendMsg("Errors", strippedErrors);
		for(var i = 0; i < strippedErrors.length; i++)
			console.error(strippedErrors[i]);
		if(useErrorOverlay) overlay.showMessage(errors);
	},
	error: function(error) {
		console.error(error);
	},
	close: function() {
		log("error", "[WDS] Disconnected!");
		sendMsg("Close");
	}
};

var hostname = urlParts.hostname;
var protocol = urlParts.protocol;


//check ipv4 and ipv6 `all hostname`
if(hostname === "0.0.0.0" || hostname === "::") {
	// why do we need this check?
	// hostname n/a for file protocol (example, when using electron, ionic)
	// see: https://github.com/webpack/webpack-dev-server/pull/384
	if(self.location.hostname && !!~self.location.protocol.indexOf("http")) {
		hostname = self.location.hostname;
	}
}

// `hostname` can be empty when the script path is relative. In that case, specifying
// a protocol would result in an invalid URL.
// When https is used in the app, secure websockets are always necessary
// because the browser doesn't accept non-secure websockets.
if(hostname && (self.location.protocol === "https:" || urlParts.hostname === "0.0.0.0")) {
	protocol = self.location.protocol;
}

var socketUrl = url.format({
	protocol: protocol,
	auth: urlParts.auth,
	hostname: hostname,
	port: (urlParts.port === "0") ? self.location.port : urlParts.port,
	pathname: urlParts.path == null || urlParts.path === "/" ? "/sockjs-node" : urlParts.path
});

socket(socketUrl, onSocketMsg);

var isUnloading = false;
self.addEventListener("beforeunload", function() {
	isUnloading = true;
});

function reloadApp() {
	if(isUnloading) {
		return;
	}
	if(hot) {
		log("info", "[WDS] App hot update...");
		var hotEmitter = __webpack_require__(185);
		hotEmitter.emit("webpackHotUpdate", currentHash);
		if(typeof self !== "undefined" && self.window) {
			// broadcast update to window
			self.postMessage("webpackHotUpdate" + currentHash, "*");
		}
	} else {
		log("info", "[WDS] App updated. Reloading...");
		self.location.reload();
	}
}

/* WEBPACK VAR INJECTION */}.call(exports, "?http://localhost:8081/"))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9Bc2lkZU1lbnVDb21wb25lbnQuanM/MTYxYyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2MwYTAiLCJ3ZWJwYWNrOi8vLyh3ZWJwYWNrKS1kZXYtc2VydmVyL2NsaWVudD8yM2RhKiIsIndlYnBhY2s6Ly8vKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50PzhiZTcqIiwid2VicGFjazovLy8od2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/ZWMyYyoiXSwibmFtZXMiOlsiQXNpZGVNZW51Q29tcG9uZW50IiwiQ29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7Ozs7Ozs7O0lBRXFCQSxrQjs7Ozs7Ozs7Ozs7MkJBQ1g7QUFDUixVQUNDO0FBQUE7QUFBQSxNQUFLLFdBQVUsZUFBZjtBQUNDO0FBQUE7QUFBQSxPQUFRLG9CQUFxQix3QkFBN0IsRUFBc0QsTUFBTSxJQUE1RCxFQUFrRSxPQUFPLEVBQXpFO0FBQ0M7QUFBQTtBQUFBO0FBQVU7QUFBVixNQUREO0FBRUM7QUFBQTtBQUFBO0FBQVU7QUFBVixNQUZEO0FBR0M7QUFBQTtBQUFBO0FBQVU7QUFBVixNQUhEO0FBSUM7QUFBQTtBQUFBO0FBQVU7QUFBVjtBQUpEO0FBREQsSUFERDtBQVVBOzs7O0VBWjhDLGdCQUFNQyxTOztrQkFBakNELGtCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSckIsdURDQUE7QUFDQSxVREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBLGdCREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBLGFEQUEsbUJDQUEsQ0RBQSxHQ0FBO0FBQ0EsY0RBQSxtQkNBQSxDREFBLEdDQUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHREFBLElDQUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsZ0JBQWdCLDZCQUE2QjtBQUM3QztBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxnQkFBZ0IsMkJBQTJCO0FBQzNDO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJEQUEsbUJDQUEsQ0RBQSxHQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDdkxBLHVEQ0FBO0FBQ0EsVURBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxnQkRBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxhREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBLGNEQUEsbUJDQUEsQ0RBQSxHQ0FBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsR0RBQSxJQ0FBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGdCQUFnQiw2QkFBNkI7QUFDN0M7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsZ0JBQWdCLDJCQUEyQjtBQUMzQztBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiMC5kMWRmZTBkMTQ2Mjc5OTk1NzlmOC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBEcmF3ZXIgZnJvbSAnbWF0ZXJpYWwtdWkvRHJhd2VyJztcbmltcG9ydCBNZW51SXRlbSBmcm9tICdtYXRlcmlhbC11aS9NZW51SXRlbSc7XG5pbXBvcnQgTGlzdEljb24gZnJvbSAncmVhY3QtbWF0ZXJpYWwtaWNvbnMvaWNvbnMvYWN0aW9uL3ZpZXctbGlzdCc7XG5pbXBvcnQgQWRkSWNvbiBmcm9tICdyZWFjdC1tYXRlcmlhbC1pY29ucy9pY29ucy9hY3Rpb24vbm90ZS1hZGQnO1xuaW1wb3J0IFNlYXJjaEljb24gZnJvbSAncmVhY3QtbWF0ZXJpYWwtaWNvbnMvaWNvbnMvYWN0aW9uL3NlYXJjaCc7XG5pbXBvcnQgRmF2b3JpdGVJY29uIGZyb20gJ3JlYWN0LW1hdGVyaWFsLWljb25zL2ljb25zL2FjdGlvbi9mYXZvcml0ZSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFzaWRlTWVudUNvbXBvbmVudCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG5cdHJlbmRlcigpIHtcblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJhc2lkZS13cmFwcGVyXCI+XG5cdFx0XHRcdDxEcmF3ZXIgY29udGFpbmVyQ2xhc3NOYW1lID0gXCJhc2lkZS13cmFwcGVyLS1vdmVybGF5XCIgb3Blbj17dHJ1ZX0gd2lkdGg9ezc1fT5cblx0XHRcdFx0XHQ8TWVudUl0ZW0+PExpc3RJY29uIC8+PC9NZW51SXRlbT5cblx0XHRcdFx0XHQ8TWVudUl0ZW0+PEFkZEljb24gLz48L01lbnVJdGVtPlxuXHRcdFx0XHRcdDxNZW51SXRlbT48U2VhcmNoSWNvbiAvPjwvTWVudUl0ZW0+XG5cdFx0XHRcdFx0PE1lbnVJdGVtPjxGYXZvcml0ZUljb24gLz48L01lbnVJdGVtPlxuXHRcdFx0XHQ8L0RyYXdlcj5cblx0XHRcdDwvZGl2PlxuXHRcdCk7XG5cdH1cbn1cblxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NvbXBvbmVudHMvQXNpZGVNZW51Q29tcG9uZW50LmpzIiwibnVsbFxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAod2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/aHR0cDovbG9jYWxob3N0OjgwODEiLCIvKiBnbG9iYWwgX19yZXNvdXJjZVF1ZXJ5ICovXHJcbnZhciB1cmwgPSByZXF1aXJlKFwidXJsXCIpO1xyXG52YXIgc3RyaXBBbnNpID0gcmVxdWlyZShcInN0cmlwLWFuc2lcIik7XHJcbnZhciBzb2NrZXQgPSByZXF1aXJlKFwiLi9zb2NrZXRcIik7XHJcbnZhciBvdmVybGF5ID0gcmVxdWlyZShcIi4vb3ZlcmxheVwiKTtcclxuXHJcbmZ1bmN0aW9uIGdldEN1cnJlbnRTY3JpcHRTb3VyY2UoKSB7XHJcblx0Ly8gYGRvY3VtZW50LmN1cnJlbnRTY3JpcHRgIGlzIHRoZSBtb3N0IGFjY3VyYXRlIHdheSB0byBmaW5kIHRoZSBjdXJyZW50IHNjcmlwdCxcclxuXHQvLyBidXQgaXMgbm90IHN1cHBvcnRlZCBpbiBhbGwgYnJvd3NlcnMuXHJcblx0aWYoZG9jdW1lbnQuY3VycmVudFNjcmlwdClcclxuXHRcdHJldHVybiBkb2N1bWVudC5jdXJyZW50U2NyaXB0LmdldEF0dHJpYnV0ZShcInNyY1wiKTtcclxuXHQvLyBGYWxsIGJhY2sgdG8gZ2V0dGluZyBhbGwgc2NyaXB0cyBpbiB0aGUgZG9jdW1lbnQuXHJcblx0dmFyIHNjcmlwdEVsZW1lbnRzID0gZG9jdW1lbnQuc2NyaXB0cyB8fCBbXTtcclxuXHR2YXIgY3VycmVudFNjcmlwdCA9IHNjcmlwdEVsZW1lbnRzW3NjcmlwdEVsZW1lbnRzLmxlbmd0aCAtIDFdO1xyXG5cdGlmKGN1cnJlbnRTY3JpcHQpXHJcblx0XHRyZXR1cm4gY3VycmVudFNjcmlwdC5nZXRBdHRyaWJ1dGUoXCJzcmNcIik7XHJcblx0Ly8gRmFpbCBhcyB0aGVyZSB3YXMgbm8gc2NyaXB0IHRvIHVzZS5cclxuXHR0aHJvdyBuZXcgRXJyb3IoXCJbV0RTXSBGYWlsZWQgdG8gZ2V0IGN1cnJlbnQgc2NyaXB0IHNvdXJjZVwiKTtcclxufVxyXG5cclxudmFyIHVybFBhcnRzO1xyXG5pZih0eXBlb2YgX19yZXNvdXJjZVF1ZXJ5ID09PSBcInN0cmluZ1wiICYmIF9fcmVzb3VyY2VRdWVyeSkge1xyXG5cdC8vIElmIHRoaXMgYnVuZGxlIGlzIGlubGluZWQsIHVzZSB0aGUgcmVzb3VyY2UgcXVlcnkgdG8gZ2V0IHRoZSBjb3JyZWN0IHVybC5cclxuXHR1cmxQYXJ0cyA9IHVybC5wYXJzZShfX3Jlc291cmNlUXVlcnkuc3Vic3RyKDEpKTtcclxufSBlbHNlIHtcclxuXHQvLyBFbHNlLCBnZXQgdGhlIHVybCBmcm9tIHRoZSA8c2NyaXB0PiB0aGlzIGZpbGUgd2FzIGNhbGxlZCB3aXRoLlxyXG5cdHZhciBzY3JpcHRIb3N0ID0gZ2V0Q3VycmVudFNjcmlwdFNvdXJjZSgpO1xyXG5cdHNjcmlwdEhvc3QgPSBzY3JpcHRIb3N0LnJlcGxhY2UoL1xcL1teXFwvXSskLywgXCJcIik7XHJcblx0dXJsUGFydHMgPSB1cmwucGFyc2UoKHNjcmlwdEhvc3QgPyBzY3JpcHRIb3N0IDogXCIvXCIpLCBmYWxzZSwgdHJ1ZSk7XHJcbn1cclxuXHJcbnZhciBob3QgPSBmYWxzZTtcclxudmFyIGluaXRpYWwgPSB0cnVlO1xyXG52YXIgY3VycmVudEhhc2ggPSBcIlwiO1xyXG52YXIgbG9nTGV2ZWwgPSBcImluZm9cIjtcclxudmFyIHVzZVdhcm5pbmdPdmVybGF5ID0gZmFsc2U7XHJcbnZhciB1c2VFcnJvck92ZXJsYXkgPSBmYWxzZTtcclxuXHJcbmZ1bmN0aW9uIGxvZyhsZXZlbCwgbXNnKSB7XHJcblx0aWYobG9nTGV2ZWwgPT09IFwiaW5mb1wiICYmIGxldmVsID09PSBcImluZm9cIilcclxuXHRcdHJldHVybiBjb25zb2xlLmxvZyhtc2cpO1xyXG5cdGlmKFtcImluZm9cIiwgXCJ3YXJuaW5nXCJdLmluZGV4T2YobG9nTGV2ZWwpID49IDAgJiYgbGV2ZWwgPT09IFwid2FybmluZ1wiKVxyXG5cdFx0cmV0dXJuIGNvbnNvbGUud2Fybihtc2cpO1xyXG5cdGlmKFtcImluZm9cIiwgXCJ3YXJuaW5nXCIsIFwiZXJyb3JcIl0uaW5kZXhPZihsb2dMZXZlbCkgPj0gMCAmJiBsZXZlbCA9PT0gXCJlcnJvclwiKVxyXG5cdFx0cmV0dXJuIGNvbnNvbGUuZXJyb3IobXNnKTtcclxufVxyXG5cclxuLy8gU2VuZCBtZXNzYWdlcyB0byB0aGUgb3V0c2lkZSwgc28gcGx1Z2lucyBjYW4gY29uc3VtZSBpdC5cclxuZnVuY3Rpb24gc2VuZE1zZyh0eXBlLCBkYXRhKSB7XHJcblx0aWYodHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgJiYgc2VsZi53aW5kb3cpIHtcclxuXHRcdHNlbGYucG9zdE1lc3NhZ2Uoe1xyXG5cdFx0XHR0eXBlOiBcIndlYnBhY2tcIiArIHR5cGUsXHJcblx0XHRcdGRhdGE6IGRhdGFcclxuXHRcdH0sIFwiKlwiKTtcclxuXHR9XHJcbn1cclxuXHJcbnZhciBvblNvY2tldE1zZyA9IHtcclxuXHRob3Q6IGZ1bmN0aW9uKCkge1xyXG5cdFx0aG90ID0gdHJ1ZTtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBIb3QgTW9kdWxlIFJlcGxhY2VtZW50IGVuYWJsZWQuXCIpO1xyXG5cdH0sXHJcblx0aW52YWxpZDogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQXBwIHVwZGF0ZWQuIFJlY29tcGlsaW5nLi4uXCIpO1xyXG5cdFx0c2VuZE1zZyhcIkludmFsaWRcIik7XHJcblx0fSxcclxuXHRoYXNoOiBmdW5jdGlvbihoYXNoKSB7XHJcblx0XHRjdXJyZW50SGFzaCA9IGhhc2g7XHJcblx0fSxcclxuXHRcInN0aWxsLW9rXCI6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIE5vdGhpbmcgY2hhbmdlZC5cIilcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5IHx8IHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5jbGVhcigpO1xyXG5cdFx0c2VuZE1zZyhcIlN0aWxsT2tcIik7XHJcblx0fSxcclxuXHRcImxvZy1sZXZlbFwiOiBmdW5jdGlvbihsZXZlbCkge1xyXG5cdFx0bG9nTGV2ZWwgPSBsZXZlbDtcclxuXHR9LFxyXG5cdFwib3ZlcmxheVwiOiBmdW5jdGlvbihvdmVybGF5KSB7XHJcblx0XHRpZih0eXBlb2YgZG9jdW1lbnQgIT09IFwidW5kZWZpbmVkXCIpIHtcclxuXHRcdFx0aWYodHlwZW9mKG92ZXJsYXkpID09PSBcImJvb2xlYW5cIikge1xyXG5cdFx0XHRcdHVzZVdhcm5pbmdPdmVybGF5ID0gb3ZlcmxheTtcclxuXHRcdFx0XHR1c2VFcnJvck92ZXJsYXkgPSBvdmVybGF5O1xyXG5cdFx0XHR9IGVsc2UgaWYob3ZlcmxheSkge1xyXG5cdFx0XHRcdHVzZVdhcm5pbmdPdmVybGF5ID0gb3ZlcmxheS53YXJuaW5ncztcclxuXHRcdFx0XHR1c2VFcnJvck92ZXJsYXkgPSBvdmVybGF5LmVycm9ycztcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0sXHJcblx0b2s6IGZ1bmN0aW9uKCkge1xyXG5cdFx0c2VuZE1zZyhcIk9rXCIpO1xyXG5cdFx0aWYodXNlV2FybmluZ092ZXJsYXkgfHwgdXNlRXJyb3JPdmVybGF5KSBvdmVybGF5LmNsZWFyKCk7XHJcblx0XHRpZihpbml0aWFsKSByZXR1cm4gaW5pdGlhbCA9IGZhbHNlO1xyXG5cdFx0cmVsb2FkQXBwKCk7XHJcblx0fSxcclxuXHRcImNvbnRlbnQtY2hhbmdlZFwiOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBDb250ZW50IGJhc2UgY2hhbmdlZC4gUmVsb2FkaW5nLi4uXCIpXHJcblx0XHRzZWxmLmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdH0sXHJcblx0d2FybmluZ3M6IGZ1bmN0aW9uKHdhcm5pbmdzKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gV2FybmluZ3Mgd2hpbGUgY29tcGlsaW5nLlwiKTtcclxuXHRcdHZhciBzdHJpcHBlZFdhcm5pbmdzID0gd2FybmluZ3MubWFwKGZ1bmN0aW9uKHdhcm5pbmcpIHtcclxuXHRcdFx0cmV0dXJuIHN0cmlwQW5zaSh3YXJuaW5nKTtcclxuXHRcdH0pO1xyXG5cdFx0c2VuZE1zZyhcIldhcm5pbmdzXCIsIHN0cmlwcGVkV2FybmluZ3MpO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHN0cmlwcGVkV2FybmluZ3MubGVuZ3RoOyBpKyspXHJcblx0XHRcdGNvbnNvbGUud2FybihzdHJpcHBlZFdhcm5pbmdzW2ldKTtcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5KSBvdmVybGF5LnNob3dNZXNzYWdlKHdhcm5pbmdzKTtcclxuXHJcblx0XHRpZihpbml0aWFsKSByZXR1cm4gaW5pdGlhbCA9IGZhbHNlO1xyXG5cdFx0cmVsb2FkQXBwKCk7XHJcblx0fSxcclxuXHRlcnJvcnM6IGZ1bmN0aW9uKGVycm9ycykge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEVycm9ycyB3aGlsZSBjb21waWxpbmcuIFJlbG9hZCBwcmV2ZW50ZWQuXCIpO1xyXG5cdFx0dmFyIHN0cmlwcGVkRXJyb3JzID0gZXJyb3JzLm1hcChmdW5jdGlvbihlcnJvcikge1xyXG5cdFx0XHRyZXR1cm4gc3RyaXBBbnNpKGVycm9yKTtcclxuXHRcdH0pO1xyXG5cdFx0c2VuZE1zZyhcIkVycm9yc1wiLCBzdHJpcHBlZEVycm9ycyk7XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgc3RyaXBwZWRFcnJvcnMubGVuZ3RoOyBpKyspXHJcblx0XHRcdGNvbnNvbGUuZXJyb3Ioc3RyaXBwZWRFcnJvcnNbaV0pO1xyXG5cdFx0aWYodXNlRXJyb3JPdmVybGF5KSBvdmVybGF5LnNob3dNZXNzYWdlKGVycm9ycyk7XHJcblx0fSxcclxuXHRlcnJvcjogZnVuY3Rpb24oZXJyb3IpIHtcclxuXHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xyXG5cdH0sXHJcblx0Y2xvc2U6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiZXJyb3JcIiwgXCJbV0RTXSBEaXNjb25uZWN0ZWQhXCIpO1xyXG5cdFx0c2VuZE1zZyhcIkNsb3NlXCIpO1xyXG5cdH1cclxufTtcclxuXHJcbnZhciBob3N0bmFtZSA9IHVybFBhcnRzLmhvc3RuYW1lO1xyXG52YXIgcHJvdG9jb2wgPSB1cmxQYXJ0cy5wcm90b2NvbDtcclxuXHJcblxyXG4vL2NoZWNrIGlwdjQgYW5kIGlwdjYgYGFsbCBob3N0bmFtZWBcclxuaWYoaG9zdG5hbWUgPT09IFwiMC4wLjAuMFwiIHx8IGhvc3RuYW1lID09PSBcIjo6XCIpIHtcclxuXHQvLyB3aHkgZG8gd2UgbmVlZCB0aGlzIGNoZWNrP1xyXG5cdC8vIGhvc3RuYW1lIG4vYSBmb3IgZmlsZSBwcm90b2NvbCAoZXhhbXBsZSwgd2hlbiB1c2luZyBlbGVjdHJvbiwgaW9uaWMpXHJcblx0Ly8gc2VlOiBodHRwczovL2dpdGh1Yi5jb20vd2VicGFjay93ZWJwYWNrLWRldi1zZXJ2ZXIvcHVsbC8zODRcclxuXHRpZihzZWxmLmxvY2F0aW9uLmhvc3RuYW1lICYmICEhfnNlbGYubG9jYXRpb24ucHJvdG9jb2wuaW5kZXhPZihcImh0dHBcIikpIHtcclxuXHRcdGhvc3RuYW1lID0gc2VsZi5sb2NhdGlvbi5ob3N0bmFtZTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIGBob3N0bmFtZWAgY2FuIGJlIGVtcHR5IHdoZW4gdGhlIHNjcmlwdCBwYXRoIGlzIHJlbGF0aXZlLiBJbiB0aGF0IGNhc2UsIHNwZWNpZnlpbmdcclxuLy8gYSBwcm90b2NvbCB3b3VsZCByZXN1bHQgaW4gYW4gaW52YWxpZCBVUkwuXHJcbi8vIFdoZW4gaHR0cHMgaXMgdXNlZCBpbiB0aGUgYXBwLCBzZWN1cmUgd2Vic29ja2V0cyBhcmUgYWx3YXlzIG5lY2Vzc2FyeVxyXG4vLyBiZWNhdXNlIHRoZSBicm93c2VyIGRvZXNuJ3QgYWNjZXB0IG5vbi1zZWN1cmUgd2Vic29ja2V0cy5cclxuaWYoaG9zdG5hbWUgJiYgKHNlbGYubG9jYXRpb24ucHJvdG9jb2wgPT09IFwiaHR0cHM6XCIgfHwgdXJsUGFydHMuaG9zdG5hbWUgPT09IFwiMC4wLjAuMFwiKSkge1xyXG5cdHByb3RvY29sID0gc2VsZi5sb2NhdGlvbi5wcm90b2NvbDtcclxufVxyXG5cclxudmFyIHNvY2tldFVybCA9IHVybC5mb3JtYXQoe1xyXG5cdHByb3RvY29sOiBwcm90b2NvbCxcclxuXHRhdXRoOiB1cmxQYXJ0cy5hdXRoLFxyXG5cdGhvc3RuYW1lOiBob3N0bmFtZSxcclxuXHRwb3J0OiAodXJsUGFydHMucG9ydCA9PT0gXCIwXCIpID8gc2VsZi5sb2NhdGlvbi5wb3J0IDogdXJsUGFydHMucG9ydCxcclxuXHRwYXRobmFtZTogdXJsUGFydHMucGF0aCA9PSBudWxsIHx8IHVybFBhcnRzLnBhdGggPT09IFwiL1wiID8gXCIvc29ja2pzLW5vZGVcIiA6IHVybFBhcnRzLnBhdGhcclxufSk7XHJcblxyXG5zb2NrZXQoc29ja2V0VXJsLCBvblNvY2tldE1zZyk7XHJcblxyXG52YXIgaXNVbmxvYWRpbmcgPSBmYWxzZTtcclxuc2VsZi5hZGRFdmVudExpc3RlbmVyKFwiYmVmb3JldW5sb2FkXCIsIGZ1bmN0aW9uKCkge1xyXG5cdGlzVW5sb2FkaW5nID0gdHJ1ZTtcclxufSk7XHJcblxyXG5mdW5jdGlvbiByZWxvYWRBcHAoKSB7XHJcblx0aWYoaXNVbmxvYWRpbmcpIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblx0aWYoaG90KSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQXBwIGhvdCB1cGRhdGUuLi5cIik7XHJcblx0XHR2YXIgaG90RW1pdHRlciA9IHJlcXVpcmUoXCJ3ZWJwYWNrL2hvdC9lbWl0dGVyXCIpO1xyXG5cdFx0aG90RW1pdHRlci5lbWl0KFwid2VicGFja0hvdFVwZGF0ZVwiLCBjdXJyZW50SGFzaCk7XHJcblx0XHRpZih0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiAmJiBzZWxmLndpbmRvdykge1xyXG5cdFx0XHQvLyBicm9hZGNhc3QgdXBkYXRlIHRvIHdpbmRvd1xyXG5cdFx0XHRzZWxmLnBvc3RNZXNzYWdlKFwid2VicGFja0hvdFVwZGF0ZVwiICsgY3VycmVudEhhc2gsIFwiKlwiKTtcclxuXHRcdH1cclxuXHR9IGVsc2Uge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCB1cGRhdGVkLiBSZWxvYWRpbmcuLi5cIik7XHJcblx0XHRzZWxmLmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdH1cclxufVxyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAod2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/aHR0cDovL2xvY2FsaG9zdDo4MDgxXG4vLyBtb2R1bGUgaWQgPSA3OTZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibnVsbFxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAod2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/aHR0cDovbG9jYWxob3N0OjgwODEvIiwiLyogZ2xvYmFsIF9fcmVzb3VyY2VRdWVyeSAqL1xyXG52YXIgdXJsID0gcmVxdWlyZShcInVybFwiKTtcclxudmFyIHN0cmlwQW5zaSA9IHJlcXVpcmUoXCJzdHJpcC1hbnNpXCIpO1xyXG52YXIgc29ja2V0ID0gcmVxdWlyZShcIi4vc29ja2V0XCIpO1xyXG52YXIgb3ZlcmxheSA9IHJlcXVpcmUoXCIuL292ZXJsYXlcIik7XHJcblxyXG5mdW5jdGlvbiBnZXRDdXJyZW50U2NyaXB0U291cmNlKCkge1xyXG5cdC8vIGBkb2N1bWVudC5jdXJyZW50U2NyaXB0YCBpcyB0aGUgbW9zdCBhY2N1cmF0ZSB3YXkgdG8gZmluZCB0aGUgY3VycmVudCBzY3JpcHQsXHJcblx0Ly8gYnV0IGlzIG5vdCBzdXBwb3J0ZWQgaW4gYWxsIGJyb3dzZXJzLlxyXG5cdGlmKGRvY3VtZW50LmN1cnJlbnRTY3JpcHQpXHJcblx0XHRyZXR1cm4gZG9jdW1lbnQuY3VycmVudFNjcmlwdC5nZXRBdHRyaWJ1dGUoXCJzcmNcIik7XHJcblx0Ly8gRmFsbCBiYWNrIHRvIGdldHRpbmcgYWxsIHNjcmlwdHMgaW4gdGhlIGRvY3VtZW50LlxyXG5cdHZhciBzY3JpcHRFbGVtZW50cyA9IGRvY3VtZW50LnNjcmlwdHMgfHwgW107XHJcblx0dmFyIGN1cnJlbnRTY3JpcHQgPSBzY3JpcHRFbGVtZW50c1tzY3JpcHRFbGVtZW50cy5sZW5ndGggLSAxXTtcclxuXHRpZihjdXJyZW50U2NyaXB0KVxyXG5cdFx0cmV0dXJuIGN1cnJlbnRTY3JpcHQuZ2V0QXR0cmlidXRlKFwic3JjXCIpO1xyXG5cdC8vIEZhaWwgYXMgdGhlcmUgd2FzIG5vIHNjcmlwdCB0byB1c2UuXHJcblx0dGhyb3cgbmV3IEVycm9yKFwiW1dEU10gRmFpbGVkIHRvIGdldCBjdXJyZW50IHNjcmlwdCBzb3VyY2VcIik7XHJcbn1cclxuXHJcbnZhciB1cmxQYXJ0cztcclxuaWYodHlwZW9mIF9fcmVzb3VyY2VRdWVyeSA9PT0gXCJzdHJpbmdcIiAmJiBfX3Jlc291cmNlUXVlcnkpIHtcclxuXHQvLyBJZiB0aGlzIGJ1bmRsZSBpcyBpbmxpbmVkLCB1c2UgdGhlIHJlc291cmNlIHF1ZXJ5IHRvIGdldCB0aGUgY29ycmVjdCB1cmwuXHJcblx0dXJsUGFydHMgPSB1cmwucGFyc2UoX19yZXNvdXJjZVF1ZXJ5LnN1YnN0cigxKSk7XHJcbn0gZWxzZSB7XHJcblx0Ly8gRWxzZSwgZ2V0IHRoZSB1cmwgZnJvbSB0aGUgPHNjcmlwdD4gdGhpcyBmaWxlIHdhcyBjYWxsZWQgd2l0aC5cclxuXHR2YXIgc2NyaXB0SG9zdCA9IGdldEN1cnJlbnRTY3JpcHRTb3VyY2UoKTtcclxuXHRzY3JpcHRIb3N0ID0gc2NyaXB0SG9zdC5yZXBsYWNlKC9cXC9bXlxcL10rJC8sIFwiXCIpO1xyXG5cdHVybFBhcnRzID0gdXJsLnBhcnNlKChzY3JpcHRIb3N0ID8gc2NyaXB0SG9zdCA6IFwiL1wiKSwgZmFsc2UsIHRydWUpO1xyXG59XHJcblxyXG52YXIgaG90ID0gZmFsc2U7XHJcbnZhciBpbml0aWFsID0gdHJ1ZTtcclxudmFyIGN1cnJlbnRIYXNoID0gXCJcIjtcclxudmFyIGxvZ0xldmVsID0gXCJpbmZvXCI7XHJcbnZhciB1c2VXYXJuaW5nT3ZlcmxheSA9IGZhbHNlO1xyXG52YXIgdXNlRXJyb3JPdmVybGF5ID0gZmFsc2U7XHJcblxyXG5mdW5jdGlvbiBsb2cobGV2ZWwsIG1zZykge1xyXG5cdGlmKGxvZ0xldmVsID09PSBcImluZm9cIiAmJiBsZXZlbCA9PT0gXCJpbmZvXCIpXHJcblx0XHRyZXR1cm4gY29uc29sZS5sb2cobXNnKTtcclxuXHRpZihbXCJpbmZvXCIsIFwid2FybmluZ1wiXS5pbmRleE9mKGxvZ0xldmVsKSA+PSAwICYmIGxldmVsID09PSBcIndhcm5pbmdcIilcclxuXHRcdHJldHVybiBjb25zb2xlLndhcm4obXNnKTtcclxuXHRpZihbXCJpbmZvXCIsIFwid2FybmluZ1wiLCBcImVycm9yXCJdLmluZGV4T2YobG9nTGV2ZWwpID49IDAgJiYgbGV2ZWwgPT09IFwiZXJyb3JcIilcclxuXHRcdHJldHVybiBjb25zb2xlLmVycm9yKG1zZyk7XHJcbn1cclxuXHJcbi8vIFNlbmQgbWVzc2FnZXMgdG8gdGhlIG91dHNpZGUsIHNvIHBsdWdpbnMgY2FuIGNvbnN1bWUgaXQuXHJcbmZ1bmN0aW9uIHNlbmRNc2codHlwZSwgZGF0YSkge1xyXG5cdGlmKHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiICYmIHNlbGYud2luZG93KSB7XHJcblx0XHRzZWxmLnBvc3RNZXNzYWdlKHtcclxuXHRcdFx0dHlwZTogXCJ3ZWJwYWNrXCIgKyB0eXBlLFxyXG5cdFx0XHRkYXRhOiBkYXRhXHJcblx0XHR9LCBcIipcIik7XHJcblx0fVxyXG59XHJcblxyXG52YXIgb25Tb2NrZXRNc2cgPSB7XHJcblx0aG90OiBmdW5jdGlvbigpIHtcclxuXHRcdGhvdCA9IHRydWU7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gSG90IE1vZHVsZSBSZXBsYWNlbWVudCBlbmFibGVkLlwiKTtcclxuXHR9LFxyXG5cdGludmFsaWQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCB1cGRhdGVkLiBSZWNvbXBpbGluZy4uLlwiKTtcclxuXHRcdHNlbmRNc2coXCJJbnZhbGlkXCIpO1xyXG5cdH0sXHJcblx0aGFzaDogZnVuY3Rpb24oaGFzaCkge1xyXG5cdFx0Y3VycmVudEhhc2ggPSBoYXNoO1xyXG5cdH0sXHJcblx0XCJzdGlsbC1va1wiOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBOb3RoaW5nIGNoYW5nZWQuXCIpXHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSB8fCB1c2VFcnJvck92ZXJsYXkpIG92ZXJsYXkuY2xlYXIoKTtcclxuXHRcdHNlbmRNc2coXCJTdGlsbE9rXCIpO1xyXG5cdH0sXHJcblx0XCJsb2ctbGV2ZWxcIjogZnVuY3Rpb24obGV2ZWwpIHtcclxuXHRcdGxvZ0xldmVsID0gbGV2ZWw7XHJcblx0fSxcclxuXHRcIm92ZXJsYXlcIjogZnVuY3Rpb24ob3ZlcmxheSkge1xyXG5cdFx0aWYodHlwZW9mIGRvY3VtZW50ICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcblx0XHRcdGlmKHR5cGVvZihvdmVybGF5KSA9PT0gXCJib29sZWFuXCIpIHtcclxuXHRcdFx0XHR1c2VXYXJuaW5nT3ZlcmxheSA9IG92ZXJsYXk7XHJcblx0XHRcdFx0dXNlRXJyb3JPdmVybGF5ID0gb3ZlcmxheTtcclxuXHRcdFx0fSBlbHNlIGlmKG92ZXJsYXkpIHtcclxuXHRcdFx0XHR1c2VXYXJuaW5nT3ZlcmxheSA9IG92ZXJsYXkud2FybmluZ3M7XHJcblx0XHRcdFx0dXNlRXJyb3JPdmVybGF5ID0gb3ZlcmxheS5lcnJvcnM7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9LFxyXG5cdG9rOiBmdW5jdGlvbigpIHtcclxuXHRcdHNlbmRNc2coXCJPa1wiKTtcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5IHx8IHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5jbGVhcigpO1xyXG5cdFx0aWYoaW5pdGlhbCkgcmV0dXJuIGluaXRpYWwgPSBmYWxzZTtcclxuXHRcdHJlbG9hZEFwcCgpO1xyXG5cdH0sXHJcblx0XCJjb250ZW50LWNoYW5nZWRcIjogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQ29udGVudCBiYXNlIGNoYW5nZWQuIFJlbG9hZGluZy4uLlwiKVxyXG5cdFx0c2VsZi5sb2NhdGlvbi5yZWxvYWQoKTtcclxuXHR9LFxyXG5cdHdhcm5pbmdzOiBmdW5jdGlvbih3YXJuaW5ncykge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIFdhcm5pbmdzIHdoaWxlIGNvbXBpbGluZy5cIik7XHJcblx0XHR2YXIgc3RyaXBwZWRXYXJuaW5ncyA9IHdhcm5pbmdzLm1hcChmdW5jdGlvbih3YXJuaW5nKSB7XHJcblx0XHRcdHJldHVybiBzdHJpcEFuc2kod2FybmluZyk7XHJcblx0XHR9KTtcclxuXHRcdHNlbmRNc2coXCJXYXJuaW5nc1wiLCBzdHJpcHBlZFdhcm5pbmdzKTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBzdHJpcHBlZFdhcm5pbmdzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRjb25zb2xlLndhcm4oc3RyaXBwZWRXYXJuaW5nc1tpXSk7XHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSkgb3ZlcmxheS5zaG93TWVzc2FnZSh3YXJuaW5ncyk7XHJcblxyXG5cdFx0aWYoaW5pdGlhbCkgcmV0dXJuIGluaXRpYWwgPSBmYWxzZTtcclxuXHRcdHJlbG9hZEFwcCgpO1xyXG5cdH0sXHJcblx0ZXJyb3JzOiBmdW5jdGlvbihlcnJvcnMpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBFcnJvcnMgd2hpbGUgY29tcGlsaW5nLiBSZWxvYWQgcHJldmVudGVkLlwiKTtcclxuXHRcdHZhciBzdHJpcHBlZEVycm9ycyA9IGVycm9ycy5tYXAoZnVuY3Rpb24oZXJyb3IpIHtcclxuXHRcdFx0cmV0dXJuIHN0cmlwQW5zaShlcnJvcik7XHJcblx0XHR9KTtcclxuXHRcdHNlbmRNc2coXCJFcnJvcnNcIiwgc3RyaXBwZWRFcnJvcnMpO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHN0cmlwcGVkRXJyb3JzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRjb25zb2xlLmVycm9yKHN0cmlwcGVkRXJyb3JzW2ldKTtcclxuXHRcdGlmKHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5zaG93TWVzc2FnZShlcnJvcnMpO1xyXG5cdH0sXHJcblx0ZXJyb3I6IGZ1bmN0aW9uKGVycm9yKSB7XHJcblx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcclxuXHR9LFxyXG5cdGNsb3NlOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImVycm9yXCIsIFwiW1dEU10gRGlzY29ubmVjdGVkIVwiKTtcclxuXHRcdHNlbmRNc2coXCJDbG9zZVwiKTtcclxuXHR9XHJcbn07XHJcblxyXG52YXIgaG9zdG5hbWUgPSB1cmxQYXJ0cy5ob3N0bmFtZTtcclxudmFyIHByb3RvY29sID0gdXJsUGFydHMucHJvdG9jb2w7XHJcblxyXG5cclxuLy9jaGVjayBpcHY0IGFuZCBpcHY2IGBhbGwgaG9zdG5hbWVgXHJcbmlmKGhvc3RuYW1lID09PSBcIjAuMC4wLjBcIiB8fCBob3N0bmFtZSA9PT0gXCI6OlwiKSB7XHJcblx0Ly8gd2h5IGRvIHdlIG5lZWQgdGhpcyBjaGVjaz9cclxuXHQvLyBob3N0bmFtZSBuL2EgZm9yIGZpbGUgcHJvdG9jb2wgKGV4YW1wbGUsIHdoZW4gdXNpbmcgZWxlY3Ryb24sIGlvbmljKVxyXG5cdC8vIHNlZTogaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2svd2VicGFjay1kZXYtc2VydmVyL3B1bGwvMzg0XHJcblx0aWYoc2VsZi5sb2NhdGlvbi5ob3N0bmFtZSAmJiAhIX5zZWxmLmxvY2F0aW9uLnByb3RvY29sLmluZGV4T2YoXCJodHRwXCIpKSB7XHJcblx0XHRob3N0bmFtZSA9IHNlbGYubG9jYXRpb24uaG9zdG5hbWU7XHJcblx0fVxyXG59XHJcblxyXG4vLyBgaG9zdG5hbWVgIGNhbiBiZSBlbXB0eSB3aGVuIHRoZSBzY3JpcHQgcGF0aCBpcyByZWxhdGl2ZS4gSW4gdGhhdCBjYXNlLCBzcGVjaWZ5aW5nXHJcbi8vIGEgcHJvdG9jb2wgd291bGQgcmVzdWx0IGluIGFuIGludmFsaWQgVVJMLlxyXG4vLyBXaGVuIGh0dHBzIGlzIHVzZWQgaW4gdGhlIGFwcCwgc2VjdXJlIHdlYnNvY2tldHMgYXJlIGFsd2F5cyBuZWNlc3NhcnlcclxuLy8gYmVjYXVzZSB0aGUgYnJvd3NlciBkb2Vzbid0IGFjY2VwdCBub24tc2VjdXJlIHdlYnNvY2tldHMuXHJcbmlmKGhvc3RuYW1lICYmIChzZWxmLmxvY2F0aW9uLnByb3RvY29sID09PSBcImh0dHBzOlwiIHx8IHVybFBhcnRzLmhvc3RuYW1lID09PSBcIjAuMC4wLjBcIikpIHtcclxuXHRwcm90b2NvbCA9IHNlbGYubG9jYXRpb24ucHJvdG9jb2w7XHJcbn1cclxuXHJcbnZhciBzb2NrZXRVcmwgPSB1cmwuZm9ybWF0KHtcclxuXHRwcm90b2NvbDogcHJvdG9jb2wsXHJcblx0YXV0aDogdXJsUGFydHMuYXV0aCxcclxuXHRob3N0bmFtZTogaG9zdG5hbWUsXHJcblx0cG9ydDogKHVybFBhcnRzLnBvcnQgPT09IFwiMFwiKSA/IHNlbGYubG9jYXRpb24ucG9ydCA6IHVybFBhcnRzLnBvcnQsXHJcblx0cGF0aG5hbWU6IHVybFBhcnRzLnBhdGggPT0gbnVsbCB8fCB1cmxQYXJ0cy5wYXRoID09PSBcIi9cIiA/IFwiL3NvY2tqcy1ub2RlXCIgOiB1cmxQYXJ0cy5wYXRoXHJcbn0pO1xyXG5cclxuc29ja2V0KHNvY2tldFVybCwgb25Tb2NrZXRNc2cpO1xyXG5cclxudmFyIGlzVW5sb2FkaW5nID0gZmFsc2U7XHJcbnNlbGYuYWRkRXZlbnRMaXN0ZW5lcihcImJlZm9yZXVubG9hZFwiLCBmdW5jdGlvbigpIHtcclxuXHRpc1VubG9hZGluZyA9IHRydWU7XHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gcmVsb2FkQXBwKCkge1xyXG5cdGlmKGlzVW5sb2FkaW5nKSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG5cdGlmKGhvdCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCBob3QgdXBkYXRlLi4uXCIpO1xyXG5cdFx0dmFyIGhvdEVtaXR0ZXIgPSByZXF1aXJlKFwid2VicGFjay9ob3QvZW1pdHRlclwiKTtcclxuXHRcdGhvdEVtaXR0ZXIuZW1pdChcIndlYnBhY2tIb3RVcGRhdGVcIiwgY3VycmVudEhhc2gpO1xyXG5cdFx0aWYodHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgJiYgc2VsZi53aW5kb3cpIHtcclxuXHRcdFx0Ly8gYnJvYWRjYXN0IHVwZGF0ZSB0byB3aW5kb3dcclxuXHRcdFx0c2VsZi5wb3N0TWVzc2FnZShcIndlYnBhY2tIb3RVcGRhdGVcIiArIGN1cnJlbnRIYXNoLCBcIipcIik7XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBBcHAgdXBkYXRlZC4gUmVsb2FkaW5nLi4uXCIpO1xyXG5cdFx0c2VsZi5sb2NhdGlvbi5yZWxvYWQoKTtcclxuXHR9XHJcbn1cclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MS9cbi8vIG1vZHVsZSBpZCA9IDc5N1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwic291cmNlUm9vdCI6IiJ9