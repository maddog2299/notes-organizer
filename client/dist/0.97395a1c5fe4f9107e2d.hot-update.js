webpackHotUpdate(0,{

/***/ 135:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(382)();
// imports


// module
exports.push([module.i, "@font-face {\n  font-family: \"Roboto\";\n  src: local(Roboto Thin), url(" + __webpack_require__(314) + ");\n  src: url(" + __webpack_require__(314) + ") format(\"embedded-opentype\"), url(" + __webpack_require__(756) + ") format(\"woff2\"), url(" + __webpack_require__(755) + ") format(\"woff\"), url(" + __webpack_require__(754) + ") format(\"truetype\");\n  font-weight: 200;\n}\n\n@font-face {\n  font-family: \"Roboto\";\n  src: local(Roboto Light), url(" + __webpack_require__(311) + ");\n  src: url(" + __webpack_require__(311) + ") format(\"embedded-opentype\"), url(" + __webpack_require__(747) + ") format(\"woff2\"), url(" + __webpack_require__(746) + ") format(\"woff\"), url(" + __webpack_require__(745) + ") format(\"truetype\");\n  font-weight: 300;\n}\n\n@font-face {\n  font-family: \"Roboto\";\n  src: local(Roboto Regular), url(" + __webpack_require__(313) + ");\n  src: url(" + __webpack_require__(313) + ") format(\"embedded-opentype\"), url(" + __webpack_require__(753) + ") format(\"woff2\"), url(" + __webpack_require__(752) + ") format(\"woff\"), url(" + __webpack_require__(751) + ") format(\"truetype\");\n  font-weight: 400;\n}\n\n@font-face {\n  font-family: \"Roboto\";\n  src: url(" + __webpack_require__(312) + ");\n  src: url(" + __webpack_require__(312) + ") format(\"embedded-opentype\"), url(" + __webpack_require__(750) + ") format(\"woff2\"), url(" + __webpack_require__(749) + ") format(\"woff\"), url(" + __webpack_require__(748) + ") format(\"truetype\");\n  font-weight: 500;\n}\n\n@font-face {\n  font-family: \"Roboto\";\n  src: url(" + __webpack_require__(310) + ");\n  src: url(" + __webpack_require__(310) + ") format(\"embedded-opentype\"), url(" + __webpack_require__(744) + ") format(\"woff2\"), url(" + __webpack_require__(743) + ") format(\"woff\"), url(" + __webpack_require__(742) + ") format(\"truetype\");\n  font-weight: 700;\n}\n\n/**\n * Created by arty on 11.05.17.\n */\n\n.menu-list {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 320:
false,

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/* global __resourceQuery */
var url = __webpack_require__(316);
var stripAnsi = __webpack_require__(309);
var socket = __webpack_require__(318);
var overlay = __webpack_require__(317);

function getCurrentScriptSource() {
	// `document.currentScript` is the most accurate way to find the current script,
	// but is not supported in all browsers.
	if(document.currentScript)
		return document.currentScript.getAttribute("src");
	// Fall back to getting all scripts in the document.
	var scriptElements = document.scripts || [];
	var currentScript = scriptElements[scriptElements.length - 1];
	if(currentScript)
		return currentScript.getAttribute("src");
	// Fail as there was no script to use.
	throw new Error("[WDS] Failed to get current script source");
}

var urlParts;
if(true) {
	// If this bundle is inlined, use the resource query to get the correct url.
	urlParts = url.parse(__resourceQuery.substr(1));
} else {
	// Else, get the url from the <script> this file was called with.
	var scriptHost = getCurrentScriptSource();
	scriptHost = scriptHost.replace(/\/[^\/]+$/, "");
	urlParts = url.parse((scriptHost ? scriptHost : "/"), false, true);
}

var hot = false;
var initial = true;
var currentHash = "";
var logLevel = "info";
var useWarningOverlay = false;
var useErrorOverlay = false;

function log(level, msg) {
	if(logLevel === "info" && level === "info")
		return console.log(msg);
	if(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning")
		return console.warn(msg);
	if(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error")
		return console.error(msg);
}

// Send messages to the outside, so plugins can consume it.
function sendMsg(type, data) {
	if(typeof self !== "undefined" && self.window) {
		self.postMessage({
			type: "webpack" + type,
			data: data
		}, "*");
	}
}

var onSocketMsg = {
	hot: function() {
		hot = true;
		log("info", "[WDS] Hot Module Replacement enabled.");
	},
	invalid: function() {
		log("info", "[WDS] App updated. Recompiling...");
		sendMsg("Invalid");
	},
	hash: function(hash) {
		currentHash = hash;
	},
	"still-ok": function() {
		log("info", "[WDS] Nothing changed.")
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		sendMsg("StillOk");
	},
	"log-level": function(level) {
		logLevel = level;
	},
	"overlay": function(overlay) {
		if(typeof document !== "undefined") {
			if(typeof(overlay) === "boolean") {
				useWarningOverlay = overlay;
				useErrorOverlay = overlay;
			} else if(overlay) {
				useWarningOverlay = overlay.warnings;
				useErrorOverlay = overlay.errors;
			}
		}
	},
	ok: function() {
		sendMsg("Ok");
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		if(initial) return initial = false;
		reloadApp();
	},
	"content-changed": function() {
		log("info", "[WDS] Content base changed. Reloading...")
		self.location.reload();
	},
	warnings: function(warnings) {
		log("info", "[WDS] Warnings while compiling.");
		var strippedWarnings = warnings.map(function(warning) {
			return stripAnsi(warning);
		});
		sendMsg("Warnings", strippedWarnings);
		for(var i = 0; i < strippedWarnings.length; i++)
			console.warn(strippedWarnings[i]);
		if(useWarningOverlay) overlay.showMessage(warnings);

		if(initial) return initial = false;
		reloadApp();
	},
	errors: function(errors) {
		log("info", "[WDS] Errors while compiling. Reload prevented.");
		var strippedErrors = errors.map(function(error) {
			return stripAnsi(error);
		});
		sendMsg("Errors", strippedErrors);
		for(var i = 0; i < strippedErrors.length; i++)
			console.error(strippedErrors[i]);
		if(useErrorOverlay) overlay.showMessage(errors);
	},
	error: function(error) {
		console.error(error);
	},
	close: function() {
		log("error", "[WDS] Disconnected!");
		sendMsg("Close");
	}
};

var hostname = urlParts.hostname;
var protocol = urlParts.protocol;


//check ipv4 and ipv6 `all hostname`
if(hostname === "0.0.0.0" || hostname === "::") {
	// why do we need this check?
	// hostname n/a for file protocol (example, when using electron, ionic)
	// see: https://github.com/webpack/webpack-dev-server/pull/384
	if(self.location.hostname && !!~self.location.protocol.indexOf("http")) {
		hostname = self.location.hostname;
	}
}

// `hostname` can be empty when the script path is relative. In that case, specifying
// a protocol would result in an invalid URL.
// When https is used in the app, secure websockets are always necessary
// because the browser doesn't accept non-secure websockets.
if(hostname && (self.location.protocol === "https:" || urlParts.hostname === "0.0.0.0")) {
	protocol = self.location.protocol;
}

var socketUrl = url.format({
	protocol: protocol,
	auth: urlParts.auth,
	hostname: hostname,
	port: (urlParts.port === "0") ? self.location.port : urlParts.port,
	pathname: urlParts.path == null || urlParts.path === "/" ? "/sockjs-node" : urlParts.path
});

socket(socketUrl, onSocketMsg);

var isUnloading = false;
self.addEventListener("beforeunload", function() {
	isUnloading = true;
});

function reloadApp() {
	if(isUnloading) {
		return;
	}
	if(hot) {
		log("info", "[WDS] App hot update...");
		var hotEmitter = __webpack_require__(183);
		hotEmitter.emit("webpackHotUpdate", currentHash);
		if(typeof self !== "undefined" && self.window) {
			// broadcast update to window
			self.postMessage("webpackHotUpdate" + currentHash, "*");
		}
	} else {
		log("info", "[WDS] App updated. Reloading...");
		self.location.reload();
	}
}

/* WEBPACK VAR INJECTION */}.call(exports, "?http://localhost:8081"))

/***/ }),

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _Drawer = __webpack_require__(224);

var _Drawer2 = _interopRequireDefault(_Drawer);

var _MenuItem = __webpack_require__(102);

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _alarm = __webpack_require__(649);

var _alarm2 = _interopRequireDefault(_alarm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AppContainer = function (_React$Component) {
  _inherits(AppContainer, _React$Component);

  function AppContainer() {
    _classCallCheck(this, AppContainer);

    return _possibleConstructorReturn(this, (AppContainer.__proto__ || Object.getPrototypeOf(AppContainer)).apply(this, arguments));
  }

  _createClass(AppContainer, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'main-wrapper' },
        _react2.default.createElement(
          'div',
          { className: 'aside-wrapper' },
          _react2.default.createElement(
            _Drawer2.default,
            { open: true, width: 75 },
            _react2.default.createElement(
              _MenuItem2.default,
              { className: 'menu-list' },
              _react2.default.createElement(_alarm2.default, null)
            ),
            _react2.default.createElement(
              _MenuItem2.default,
              { className: 'menu-list' },
              'Menu Item 2'
            )
          )
        )
      );
    }
  }]);

  return AppContainer;
}(_react2.default.Component);

exports.default = AppContainer;

/***/ }),

/***/ 762:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(321);
__webpack_require__(322);
__webpack_require__(763);
module.exports = __webpack_require__(319);


/***/ }),

/***/ 763:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/* global __resourceQuery */
var url = __webpack_require__(316);
var stripAnsi = __webpack_require__(309);
var socket = __webpack_require__(318);
var overlay = __webpack_require__(317);

function getCurrentScriptSource() {
	// `document.currentScript` is the most accurate way to find the current script,
	// but is not supported in all browsers.
	if(document.currentScript)
		return document.currentScript.getAttribute("src");
	// Fall back to getting all scripts in the document.
	var scriptElements = document.scripts || [];
	var currentScript = scriptElements[scriptElements.length - 1];
	if(currentScript)
		return currentScript.getAttribute("src");
	// Fail as there was no script to use.
	throw new Error("[WDS] Failed to get current script source");
}

var urlParts;
if(true) {
	// If this bundle is inlined, use the resource query to get the correct url.
	urlParts = url.parse(__resourceQuery.substr(1));
} else {
	// Else, get the url from the <script> this file was called with.
	var scriptHost = getCurrentScriptSource();
	scriptHost = scriptHost.replace(/\/[^\/]+$/, "");
	urlParts = url.parse((scriptHost ? scriptHost : "/"), false, true);
}

var hot = false;
var initial = true;
var currentHash = "";
var logLevel = "info";
var useWarningOverlay = false;
var useErrorOverlay = false;

function log(level, msg) {
	if(logLevel === "info" && level === "info")
		return console.log(msg);
	if(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning")
		return console.warn(msg);
	if(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error")
		return console.error(msg);
}

// Send messages to the outside, so plugins can consume it.
function sendMsg(type, data) {
	if(typeof self !== "undefined" && self.window) {
		self.postMessage({
			type: "webpack" + type,
			data: data
		}, "*");
	}
}

var onSocketMsg = {
	hot: function() {
		hot = true;
		log("info", "[WDS] Hot Module Replacement enabled.");
	},
	invalid: function() {
		log("info", "[WDS] App updated. Recompiling...");
		sendMsg("Invalid");
	},
	hash: function(hash) {
		currentHash = hash;
	},
	"still-ok": function() {
		log("info", "[WDS] Nothing changed.")
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		sendMsg("StillOk");
	},
	"log-level": function(level) {
		logLevel = level;
	},
	"overlay": function(overlay) {
		if(typeof document !== "undefined") {
			if(typeof(overlay) === "boolean") {
				useWarningOverlay = overlay;
				useErrorOverlay = overlay;
			} else if(overlay) {
				useWarningOverlay = overlay.warnings;
				useErrorOverlay = overlay.errors;
			}
		}
	},
	ok: function() {
		sendMsg("Ok");
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		if(initial) return initial = false;
		reloadApp();
	},
	"content-changed": function() {
		log("info", "[WDS] Content base changed. Reloading...")
		self.location.reload();
	},
	warnings: function(warnings) {
		log("info", "[WDS] Warnings while compiling.");
		var strippedWarnings = warnings.map(function(warning) {
			return stripAnsi(warning);
		});
		sendMsg("Warnings", strippedWarnings);
		for(var i = 0; i < strippedWarnings.length; i++)
			console.warn(strippedWarnings[i]);
		if(useWarningOverlay) overlay.showMessage(warnings);

		if(initial) return initial = false;
		reloadApp();
	},
	errors: function(errors) {
		log("info", "[WDS] Errors while compiling. Reload prevented.");
		var strippedErrors = errors.map(function(error) {
			return stripAnsi(error);
		});
		sendMsg("Errors", strippedErrors);
		for(var i = 0; i < strippedErrors.length; i++)
			console.error(strippedErrors[i]);
		if(useErrorOverlay) overlay.showMessage(errors);
	},
	error: function(error) {
		console.error(error);
	},
	close: function() {
		log("error", "[WDS] Disconnected!");
		sendMsg("Close");
	}
};

var hostname = urlParts.hostname;
var protocol = urlParts.protocol;


//check ipv4 and ipv6 `all hostname`
if(hostname === "0.0.0.0" || hostname === "::") {
	// why do we need this check?
	// hostname n/a for file protocol (example, when using electron, ionic)
	// see: https://github.com/webpack/webpack-dev-server/pull/384
	if(self.location.hostname && !!~self.location.protocol.indexOf("http")) {
		hostname = self.location.hostname;
	}
}

// `hostname` can be empty when the script path is relative. In that case, specifying
// a protocol would result in an invalid URL.
// When https is used in the app, secure websockets are always necessary
// because the browser doesn't accept non-secure websockets.
if(hostname && (self.location.protocol === "https:" || urlParts.hostname === "0.0.0.0")) {
	protocol = self.location.protocol;
}

var socketUrl = url.format({
	protocol: protocol,
	auth: urlParts.auth,
	hostname: hostname,
	port: (urlParts.port === "0") ? self.location.port : urlParts.port,
	pathname: urlParts.path == null || urlParts.path === "/" ? "/sockjs-node" : urlParts.path
});

socket(socketUrl, onSocketMsg);

var isUnloading = false;
self.addEventListener("beforeunload", function() {
	isUnloading = true;
});

function reloadApp() {
	if(isUnloading) {
		return;
	}
	if(hot) {
		log("info", "[WDS] App hot update...");
		var hotEmitter = __webpack_require__(183);
		hotEmitter.emit("webpackHotUpdate", currentHash);
		if(typeof self !== "undefined" && self.window) {
			// broadcast update to window
			self.postMessage("webpackHotUpdate" + currentHash, "*");
		}
	} else {
		log("info", "[WDS] App updated. Reloading...");
		self.location.reload();
	}
}

/* WEBPACK VAR INJECTION */}.call(exports, "?http://localhost:8081/"))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL3N0eWxlcy9tYWluLnNjc3M/M2JhNCIsIndlYnBhY2s6Ly8vKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2MwYTAiLCJ3ZWJwYWNrOi8vLyh3ZWJwYWNrKS1kZXYtc2VydmVyL2NsaWVudD8yM2RhKiIsIndlYnBhY2s6Ly8vLi9zcmMvY29udGFpbmVycy9BcHBDb250YWluZXIuanM/NDIwNSIsIndlYnBhY2s6Ly8vKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50PzhiZTcqIiwid2VicGFjazovLy8od2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/ZWMyYyoiXSwibmFtZXMiOlsiQXBwQ29udGFpbmVyIiwiQ29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7OztBQUdBO0FBQ0EscUNBQXNDLDRCQUE0QixtRUFBcUYsMFBBQXFVLHFCQUFxQixHQUFHLGdCQUFnQiw0QkFBNEIsb0VBQXVGLDBQQUF5VSxxQkFBcUIsR0FBRyxnQkFBZ0IsNEJBQTRCLHNFQUEyRiwwUEFBaVYscUJBQXFCLEdBQUcsZ0JBQWdCLDRCQUE0QiwrQ0FBbUUsMFBBQTZVLHFCQUFxQixHQUFHLGdCQUFnQiw0QkFBNEIsK0NBQWlFLDBQQUFxVSxxQkFBcUIsR0FBRyw2REFBNkQsa0JBQWtCLHdCQUF3Qiw0QkFBNEIsR0FBRzs7QUFFNStFOzs7Ozs7Ozs7OztBQ1BBLHVEQ0FBO0FBQ0EsVURBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxnQkRBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxhREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBLGNEQUEsbUJDQUEsQ0RBQSxHQ0FBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsR0RBQSxJQ0FBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGdCQUFnQiw2QkFBNkI7QUFDN0M7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsZ0JBQWdCLDJCQUEyQjtBQUMzQztBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZMQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7Ozs7Ozs7Ozs7O0lBRU1BLFk7Ozs7Ozs7Ozs7OzZCQUNLO0FBQ1AsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFVLGNBQWY7QUFDRTtBQUFBO0FBQUEsWUFBSyxXQUFVLGVBQWY7QUFDRTtBQUFBO0FBQUEsY0FBUSxNQUFNLElBQWQsRUFBb0IsT0FBTyxFQUEzQjtBQUNFO0FBQUE7QUFBQSxnQkFBVSxXQUFVLFdBQXBCO0FBQWdDO0FBQWhDLGFBREY7QUFFRTtBQUFBO0FBQUEsZ0JBQVUsV0FBVSxXQUFwQjtBQUFBO0FBQUE7QUFGRjtBQURGO0FBREYsT0FERjtBQVVEOzs7O0VBWndCLGdCQUFNQyxTOztrQkFlbEJELFk7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3JCZix1RENBQTtBQUNBLFVEQUEsbUJDQUEsQ0RBQSxHQ0FBO0FBQ0EsZ0JEQUEsbUJDQUEsQ0RBQSxHQ0FBO0FBQ0EsYURBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxjREFBLG1CQ0FBLENEQUEsR0NBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdEQUEsSUNBQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxnQkFBZ0IsNkJBQTZCO0FBQzdDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGdCQUFnQiwyQkFBMkI7QUFDM0M7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkRBQSxtQkNBQSxDREFBLEdDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6IjAuOTczOTVhMWM1ZmU0ZjkxMDdlMmQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJSb2JvdG9cXFwiO1xcbiAgc3JjOiBsb2NhbChSb2JvdG8gVGhpbiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLVRoaW4uZW90XCIpICsgXCIpO1xcbiAgc3JjOiB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1UaGluLmVvdFwiKSArIFwiKSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLVRoaW4ud29mZjJcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmMlxcXCIpLCB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1UaGluLndvZmZcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLVRoaW4udHRmXCIpICsgXCIpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAyMDA7XFxufVxcblxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJSb2JvdG9cXFwiO1xcbiAgc3JjOiBsb2NhbChSb2JvdG8gTGlnaHQpLCB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1MaWdodC5lb3RcIikgKyBcIik7XFxuICBzcmM6IHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLUxpZ2h0LmVvdFwiKSArIFwiKSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLUxpZ2h0LndvZmYyXCIpICsgXCIpIGZvcm1hdChcXFwid29mZjJcXFwiKSwgdXJsKFwiICsgcmVxdWlyZShcIi4uL2ZvbnRzL3JvYm90by9Sb2JvdG8tTGlnaHQud29mZlwiKSArIFwiKSBmb3JtYXQoXFxcIndvZmZcXFwiKSwgdXJsKFwiICsgcmVxdWlyZShcIi4uL2ZvbnRzL3JvYm90by9Sb2JvdG8tTGlnaHQudHRmXCIpICsgXCIpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiAzMDA7XFxufVxcblxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJSb2JvdG9cXFwiO1xcbiAgc3JjOiBsb2NhbChSb2JvdG8gUmVndWxhciksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLVJlZ3VsYXIuZW90XCIpICsgXCIpO1xcbiAgc3JjOiB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1SZWd1bGFyLmVvdFwiKSArIFwiKSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLVJlZ3VsYXIud29mZjJcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmMlxcXCIpLCB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1SZWd1bGFyLndvZmZcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLVJlZ3VsYXIudHRmXCIpICsgXCIpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XFxufVxcblxcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJSb2JvdG9cXFwiO1xcbiAgc3JjOiB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1NZWRpdW0uZW90XCIpICsgXCIpO1xcbiAgc3JjOiB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1NZWRpdW0uZW90XCIpICsgXCIpIGZvcm1hdChcXFwiZW1iZWRkZWQtb3BlbnR5cGVcXFwiKSwgdXJsKFwiICsgcmVxdWlyZShcIi4uL2ZvbnRzL3JvYm90by9Sb2JvdG8tTWVkaXVtLndvZmYyXCIpICsgXCIpIGZvcm1hdChcXFwid29mZjJcXFwiKSwgdXJsKFwiICsgcmVxdWlyZShcIi4uL2ZvbnRzL3JvYm90by9Sb2JvdG8tTWVkaXVtLndvZmZcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLU1lZGl1bS50dGZcIikgKyBcIikgZm9ybWF0KFxcXCJ0cnVldHlwZVxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG59XFxuXFxuQGZvbnQtZmFjZSB7XFxuICBmb250LWZhbWlseTogXFxcIlJvYm90b1xcXCI7XFxuICBzcmM6IHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLUJvbGQuZW90XCIpICsgXCIpO1xcbiAgc3JjOiB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1Cb2xkLmVvdFwiKSArIFwiKSBmb3JtYXQoXFxcImVtYmVkZGVkLW9wZW50eXBlXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLUJvbGQud29mZjJcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmMlxcXCIpLCB1cmwoXCIgKyByZXF1aXJlKFwiLi4vZm9udHMvcm9ib3RvL1JvYm90by1Cb2xkLndvZmZcIikgKyBcIikgZm9ybWF0KFxcXCJ3b2ZmXFxcIiksIHVybChcIiArIHJlcXVpcmUoXCIuLi9mb250cy9yb2JvdG8vUm9ib3RvLUJvbGQudHRmXCIpICsgXCIpIGZvcm1hdChcXFwidHJ1ZXR5cGVcXFwiKTtcXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XFxufVxcblxcbi8qKlxcbiAqIENyZWF0ZWQgYnkgYXJ0eSBvbiAxMS4wNS4xNy5cXG4gKi9cXG5cXG4ubWVudS1saXN0IHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxufVxcblxcblwiLCBcIlwiXSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi9jc3MtbG9hZGVyIS4vfi9yZXNvbHZlLXVybC1sb2FkZXIhLi9+L3Nhc3MtbG9hZGVyP3NvdXJjZU1hcCEuL3NyYy9hc3NldHMvc3R5bGVzL21haW4uc2Nzc1xuLy8gbW9kdWxlIGlkID0gMTM1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIm51bGxcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2h0dHA6L2xvY2FsaG9zdDo4MDgxIiwiLyogZ2xvYmFsIF9fcmVzb3VyY2VRdWVyeSAqL1xyXG52YXIgdXJsID0gcmVxdWlyZShcInVybFwiKTtcclxudmFyIHN0cmlwQW5zaSA9IHJlcXVpcmUoXCJzdHJpcC1hbnNpXCIpO1xyXG52YXIgc29ja2V0ID0gcmVxdWlyZShcIi4vc29ja2V0XCIpO1xyXG52YXIgb3ZlcmxheSA9IHJlcXVpcmUoXCIuL292ZXJsYXlcIik7XHJcblxyXG5mdW5jdGlvbiBnZXRDdXJyZW50U2NyaXB0U291cmNlKCkge1xyXG5cdC8vIGBkb2N1bWVudC5jdXJyZW50U2NyaXB0YCBpcyB0aGUgbW9zdCBhY2N1cmF0ZSB3YXkgdG8gZmluZCB0aGUgY3VycmVudCBzY3JpcHQsXHJcblx0Ly8gYnV0IGlzIG5vdCBzdXBwb3J0ZWQgaW4gYWxsIGJyb3dzZXJzLlxyXG5cdGlmKGRvY3VtZW50LmN1cnJlbnRTY3JpcHQpXHJcblx0XHRyZXR1cm4gZG9jdW1lbnQuY3VycmVudFNjcmlwdC5nZXRBdHRyaWJ1dGUoXCJzcmNcIik7XHJcblx0Ly8gRmFsbCBiYWNrIHRvIGdldHRpbmcgYWxsIHNjcmlwdHMgaW4gdGhlIGRvY3VtZW50LlxyXG5cdHZhciBzY3JpcHRFbGVtZW50cyA9IGRvY3VtZW50LnNjcmlwdHMgfHwgW107XHJcblx0dmFyIGN1cnJlbnRTY3JpcHQgPSBzY3JpcHRFbGVtZW50c1tzY3JpcHRFbGVtZW50cy5sZW5ndGggLSAxXTtcclxuXHRpZihjdXJyZW50U2NyaXB0KVxyXG5cdFx0cmV0dXJuIGN1cnJlbnRTY3JpcHQuZ2V0QXR0cmlidXRlKFwic3JjXCIpO1xyXG5cdC8vIEZhaWwgYXMgdGhlcmUgd2FzIG5vIHNjcmlwdCB0byB1c2UuXHJcblx0dGhyb3cgbmV3IEVycm9yKFwiW1dEU10gRmFpbGVkIHRvIGdldCBjdXJyZW50IHNjcmlwdCBzb3VyY2VcIik7XHJcbn1cclxuXHJcbnZhciB1cmxQYXJ0cztcclxuaWYodHlwZW9mIF9fcmVzb3VyY2VRdWVyeSA9PT0gXCJzdHJpbmdcIiAmJiBfX3Jlc291cmNlUXVlcnkpIHtcclxuXHQvLyBJZiB0aGlzIGJ1bmRsZSBpcyBpbmxpbmVkLCB1c2UgdGhlIHJlc291cmNlIHF1ZXJ5IHRvIGdldCB0aGUgY29ycmVjdCB1cmwuXHJcblx0dXJsUGFydHMgPSB1cmwucGFyc2UoX19yZXNvdXJjZVF1ZXJ5LnN1YnN0cigxKSk7XHJcbn0gZWxzZSB7XHJcblx0Ly8gRWxzZSwgZ2V0IHRoZSB1cmwgZnJvbSB0aGUgPHNjcmlwdD4gdGhpcyBmaWxlIHdhcyBjYWxsZWQgd2l0aC5cclxuXHR2YXIgc2NyaXB0SG9zdCA9IGdldEN1cnJlbnRTY3JpcHRTb3VyY2UoKTtcclxuXHRzY3JpcHRIb3N0ID0gc2NyaXB0SG9zdC5yZXBsYWNlKC9cXC9bXlxcL10rJC8sIFwiXCIpO1xyXG5cdHVybFBhcnRzID0gdXJsLnBhcnNlKChzY3JpcHRIb3N0ID8gc2NyaXB0SG9zdCA6IFwiL1wiKSwgZmFsc2UsIHRydWUpO1xyXG59XHJcblxyXG52YXIgaG90ID0gZmFsc2U7XHJcbnZhciBpbml0aWFsID0gdHJ1ZTtcclxudmFyIGN1cnJlbnRIYXNoID0gXCJcIjtcclxudmFyIGxvZ0xldmVsID0gXCJpbmZvXCI7XHJcbnZhciB1c2VXYXJuaW5nT3ZlcmxheSA9IGZhbHNlO1xyXG52YXIgdXNlRXJyb3JPdmVybGF5ID0gZmFsc2U7XHJcblxyXG5mdW5jdGlvbiBsb2cobGV2ZWwsIG1zZykge1xyXG5cdGlmKGxvZ0xldmVsID09PSBcImluZm9cIiAmJiBsZXZlbCA9PT0gXCJpbmZvXCIpXHJcblx0XHRyZXR1cm4gY29uc29sZS5sb2cobXNnKTtcclxuXHRpZihbXCJpbmZvXCIsIFwid2FybmluZ1wiXS5pbmRleE9mKGxvZ0xldmVsKSA+PSAwICYmIGxldmVsID09PSBcIndhcm5pbmdcIilcclxuXHRcdHJldHVybiBjb25zb2xlLndhcm4obXNnKTtcclxuXHRpZihbXCJpbmZvXCIsIFwid2FybmluZ1wiLCBcImVycm9yXCJdLmluZGV4T2YobG9nTGV2ZWwpID49IDAgJiYgbGV2ZWwgPT09IFwiZXJyb3JcIilcclxuXHRcdHJldHVybiBjb25zb2xlLmVycm9yKG1zZyk7XHJcbn1cclxuXHJcbi8vIFNlbmQgbWVzc2FnZXMgdG8gdGhlIG91dHNpZGUsIHNvIHBsdWdpbnMgY2FuIGNvbnN1bWUgaXQuXHJcbmZ1bmN0aW9uIHNlbmRNc2codHlwZSwgZGF0YSkge1xyXG5cdGlmKHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiICYmIHNlbGYud2luZG93KSB7XHJcblx0XHRzZWxmLnBvc3RNZXNzYWdlKHtcclxuXHRcdFx0dHlwZTogXCJ3ZWJwYWNrXCIgKyB0eXBlLFxyXG5cdFx0XHRkYXRhOiBkYXRhXHJcblx0XHR9LCBcIipcIik7XHJcblx0fVxyXG59XHJcblxyXG52YXIgb25Tb2NrZXRNc2cgPSB7XHJcblx0aG90OiBmdW5jdGlvbigpIHtcclxuXHRcdGhvdCA9IHRydWU7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gSG90IE1vZHVsZSBSZXBsYWNlbWVudCBlbmFibGVkLlwiKTtcclxuXHR9LFxyXG5cdGludmFsaWQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCB1cGRhdGVkLiBSZWNvbXBpbGluZy4uLlwiKTtcclxuXHRcdHNlbmRNc2coXCJJbnZhbGlkXCIpO1xyXG5cdH0sXHJcblx0aGFzaDogZnVuY3Rpb24oaGFzaCkge1xyXG5cdFx0Y3VycmVudEhhc2ggPSBoYXNoO1xyXG5cdH0sXHJcblx0XCJzdGlsbC1va1wiOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBOb3RoaW5nIGNoYW5nZWQuXCIpXHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSB8fCB1c2VFcnJvck92ZXJsYXkpIG92ZXJsYXkuY2xlYXIoKTtcclxuXHRcdHNlbmRNc2coXCJTdGlsbE9rXCIpO1xyXG5cdH0sXHJcblx0XCJsb2ctbGV2ZWxcIjogZnVuY3Rpb24obGV2ZWwpIHtcclxuXHRcdGxvZ0xldmVsID0gbGV2ZWw7XHJcblx0fSxcclxuXHRcIm92ZXJsYXlcIjogZnVuY3Rpb24ob3ZlcmxheSkge1xyXG5cdFx0aWYodHlwZW9mIGRvY3VtZW50ICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcblx0XHRcdGlmKHR5cGVvZihvdmVybGF5KSA9PT0gXCJib29sZWFuXCIpIHtcclxuXHRcdFx0XHR1c2VXYXJuaW5nT3ZlcmxheSA9IG92ZXJsYXk7XHJcblx0XHRcdFx0dXNlRXJyb3JPdmVybGF5ID0gb3ZlcmxheTtcclxuXHRcdFx0fSBlbHNlIGlmKG92ZXJsYXkpIHtcclxuXHRcdFx0XHR1c2VXYXJuaW5nT3ZlcmxheSA9IG92ZXJsYXkud2FybmluZ3M7XHJcblx0XHRcdFx0dXNlRXJyb3JPdmVybGF5ID0gb3ZlcmxheS5lcnJvcnM7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9LFxyXG5cdG9rOiBmdW5jdGlvbigpIHtcclxuXHRcdHNlbmRNc2coXCJPa1wiKTtcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5IHx8IHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5jbGVhcigpO1xyXG5cdFx0aWYoaW5pdGlhbCkgcmV0dXJuIGluaXRpYWwgPSBmYWxzZTtcclxuXHRcdHJlbG9hZEFwcCgpO1xyXG5cdH0sXHJcblx0XCJjb250ZW50LWNoYW5nZWRcIjogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQ29udGVudCBiYXNlIGNoYW5nZWQuIFJlbG9hZGluZy4uLlwiKVxyXG5cdFx0c2VsZi5sb2NhdGlvbi5yZWxvYWQoKTtcclxuXHR9LFxyXG5cdHdhcm5pbmdzOiBmdW5jdGlvbih3YXJuaW5ncykge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIFdhcm5pbmdzIHdoaWxlIGNvbXBpbGluZy5cIik7XHJcblx0XHR2YXIgc3RyaXBwZWRXYXJuaW5ncyA9IHdhcm5pbmdzLm1hcChmdW5jdGlvbih3YXJuaW5nKSB7XHJcblx0XHRcdHJldHVybiBzdHJpcEFuc2kod2FybmluZyk7XHJcblx0XHR9KTtcclxuXHRcdHNlbmRNc2coXCJXYXJuaW5nc1wiLCBzdHJpcHBlZFdhcm5pbmdzKTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBzdHJpcHBlZFdhcm5pbmdzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRjb25zb2xlLndhcm4oc3RyaXBwZWRXYXJuaW5nc1tpXSk7XHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSkgb3ZlcmxheS5zaG93TWVzc2FnZSh3YXJuaW5ncyk7XHJcblxyXG5cdFx0aWYoaW5pdGlhbCkgcmV0dXJuIGluaXRpYWwgPSBmYWxzZTtcclxuXHRcdHJlbG9hZEFwcCgpO1xyXG5cdH0sXHJcblx0ZXJyb3JzOiBmdW5jdGlvbihlcnJvcnMpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBFcnJvcnMgd2hpbGUgY29tcGlsaW5nLiBSZWxvYWQgcHJldmVudGVkLlwiKTtcclxuXHRcdHZhciBzdHJpcHBlZEVycm9ycyA9IGVycm9ycy5tYXAoZnVuY3Rpb24oZXJyb3IpIHtcclxuXHRcdFx0cmV0dXJuIHN0cmlwQW5zaShlcnJvcik7XHJcblx0XHR9KTtcclxuXHRcdHNlbmRNc2coXCJFcnJvcnNcIiwgc3RyaXBwZWRFcnJvcnMpO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHN0cmlwcGVkRXJyb3JzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRjb25zb2xlLmVycm9yKHN0cmlwcGVkRXJyb3JzW2ldKTtcclxuXHRcdGlmKHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5zaG93TWVzc2FnZShlcnJvcnMpO1xyXG5cdH0sXHJcblx0ZXJyb3I6IGZ1bmN0aW9uKGVycm9yKSB7XHJcblx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcclxuXHR9LFxyXG5cdGNsb3NlOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImVycm9yXCIsIFwiW1dEU10gRGlzY29ubmVjdGVkIVwiKTtcclxuXHRcdHNlbmRNc2coXCJDbG9zZVwiKTtcclxuXHR9XHJcbn07XHJcblxyXG52YXIgaG9zdG5hbWUgPSB1cmxQYXJ0cy5ob3N0bmFtZTtcclxudmFyIHByb3RvY29sID0gdXJsUGFydHMucHJvdG9jb2w7XHJcblxyXG5cclxuLy9jaGVjayBpcHY0IGFuZCBpcHY2IGBhbGwgaG9zdG5hbWVgXHJcbmlmKGhvc3RuYW1lID09PSBcIjAuMC4wLjBcIiB8fCBob3N0bmFtZSA9PT0gXCI6OlwiKSB7XHJcblx0Ly8gd2h5IGRvIHdlIG5lZWQgdGhpcyBjaGVjaz9cclxuXHQvLyBob3N0bmFtZSBuL2EgZm9yIGZpbGUgcHJvdG9jb2wgKGV4YW1wbGUsIHdoZW4gdXNpbmcgZWxlY3Ryb24sIGlvbmljKVxyXG5cdC8vIHNlZTogaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2svd2VicGFjay1kZXYtc2VydmVyL3B1bGwvMzg0XHJcblx0aWYoc2VsZi5sb2NhdGlvbi5ob3N0bmFtZSAmJiAhIX5zZWxmLmxvY2F0aW9uLnByb3RvY29sLmluZGV4T2YoXCJodHRwXCIpKSB7XHJcblx0XHRob3N0bmFtZSA9IHNlbGYubG9jYXRpb24uaG9zdG5hbWU7XHJcblx0fVxyXG59XHJcblxyXG4vLyBgaG9zdG5hbWVgIGNhbiBiZSBlbXB0eSB3aGVuIHRoZSBzY3JpcHQgcGF0aCBpcyByZWxhdGl2ZS4gSW4gdGhhdCBjYXNlLCBzcGVjaWZ5aW5nXHJcbi8vIGEgcHJvdG9jb2wgd291bGQgcmVzdWx0IGluIGFuIGludmFsaWQgVVJMLlxyXG4vLyBXaGVuIGh0dHBzIGlzIHVzZWQgaW4gdGhlIGFwcCwgc2VjdXJlIHdlYnNvY2tldHMgYXJlIGFsd2F5cyBuZWNlc3NhcnlcclxuLy8gYmVjYXVzZSB0aGUgYnJvd3NlciBkb2Vzbid0IGFjY2VwdCBub24tc2VjdXJlIHdlYnNvY2tldHMuXHJcbmlmKGhvc3RuYW1lICYmIChzZWxmLmxvY2F0aW9uLnByb3RvY29sID09PSBcImh0dHBzOlwiIHx8IHVybFBhcnRzLmhvc3RuYW1lID09PSBcIjAuMC4wLjBcIikpIHtcclxuXHRwcm90b2NvbCA9IHNlbGYubG9jYXRpb24ucHJvdG9jb2w7XHJcbn1cclxuXHJcbnZhciBzb2NrZXRVcmwgPSB1cmwuZm9ybWF0KHtcclxuXHRwcm90b2NvbDogcHJvdG9jb2wsXHJcblx0YXV0aDogdXJsUGFydHMuYXV0aCxcclxuXHRob3N0bmFtZTogaG9zdG5hbWUsXHJcblx0cG9ydDogKHVybFBhcnRzLnBvcnQgPT09IFwiMFwiKSA/IHNlbGYubG9jYXRpb24ucG9ydCA6IHVybFBhcnRzLnBvcnQsXHJcblx0cGF0aG5hbWU6IHVybFBhcnRzLnBhdGggPT0gbnVsbCB8fCB1cmxQYXJ0cy5wYXRoID09PSBcIi9cIiA/IFwiL3NvY2tqcy1ub2RlXCIgOiB1cmxQYXJ0cy5wYXRoXHJcbn0pO1xyXG5cclxuc29ja2V0KHNvY2tldFVybCwgb25Tb2NrZXRNc2cpO1xyXG5cclxudmFyIGlzVW5sb2FkaW5nID0gZmFsc2U7XHJcbnNlbGYuYWRkRXZlbnRMaXN0ZW5lcihcImJlZm9yZXVubG9hZFwiLCBmdW5jdGlvbigpIHtcclxuXHRpc1VubG9hZGluZyA9IHRydWU7XHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gcmVsb2FkQXBwKCkge1xyXG5cdGlmKGlzVW5sb2FkaW5nKSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG5cdGlmKGhvdCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCBob3QgdXBkYXRlLi4uXCIpO1xyXG5cdFx0dmFyIGhvdEVtaXR0ZXIgPSByZXF1aXJlKFwid2VicGFjay9ob3QvZW1pdHRlclwiKTtcclxuXHRcdGhvdEVtaXR0ZXIuZW1pdChcIndlYnBhY2tIb3RVcGRhdGVcIiwgY3VycmVudEhhc2gpO1xyXG5cdFx0aWYodHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgJiYgc2VsZi53aW5kb3cpIHtcclxuXHRcdFx0Ly8gYnJvYWRjYXN0IHVwZGF0ZSB0byB3aW5kb3dcclxuXHRcdFx0c2VsZi5wb3N0TWVzc2FnZShcIndlYnBhY2tIb3RVcGRhdGVcIiArIGN1cnJlbnRIYXNoLCBcIipcIik7XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBBcHAgdXBkYXRlZC4gUmVsb2FkaW5nLi4uXCIpO1xyXG5cdFx0c2VsZi5sb2NhdGlvbi5yZWxvYWQoKTtcclxuXHR9XHJcbn1cclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MVxuLy8gbW9kdWxlIGlkID0gMzIxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgRHJhd2VyIGZyb20gJ21hdGVyaWFsLXVpL0RyYXdlcic7XG5pbXBvcnQgTWVudUl0ZW0gZnJvbSAnbWF0ZXJpYWwtdWkvTWVudUl0ZW0nO1xuXG5pbXBvcnQgQWxhcm1JY29uIGZyb20gJ3JlYWN0LW1hdGVyaWFsLWljb25zL2ljb25zL2FjdGlvbi9hbGFybSc7XG5cbmNsYXNzIEFwcENvbnRhaW5lciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtYWluLXdyYXBwZXJcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhc2lkZS13cmFwcGVyXCI+XG4gICAgICAgICAgPERyYXdlciBvcGVuPXt0cnVlfSB3aWR0aD17NzV9PlxuICAgICAgICAgICAgPE1lbnVJdGVtIGNsYXNzTmFtZT1cIm1lbnUtbGlzdFwiPjxBbGFybUljb24vPjwvTWVudUl0ZW0+XG4gICAgICAgICAgICA8TWVudUl0ZW0gY2xhc3NOYW1lPVwibWVudS1saXN0XCI+TWVudSBJdGVtIDI8L01lbnVJdGVtPlxuICAgICAgICAgIDwvRHJhd2VyPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQXBwQ29udGFpbmVyO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NvbnRhaW5lcnMvQXBwQ29udGFpbmVyLmpzIiwibnVsbFxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAod2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/aHR0cDovbG9jYWxob3N0OjgwODEvIiwiLyogZ2xvYmFsIF9fcmVzb3VyY2VRdWVyeSAqL1xyXG52YXIgdXJsID0gcmVxdWlyZShcInVybFwiKTtcclxudmFyIHN0cmlwQW5zaSA9IHJlcXVpcmUoXCJzdHJpcC1hbnNpXCIpO1xyXG52YXIgc29ja2V0ID0gcmVxdWlyZShcIi4vc29ja2V0XCIpO1xyXG52YXIgb3ZlcmxheSA9IHJlcXVpcmUoXCIuL292ZXJsYXlcIik7XHJcblxyXG5mdW5jdGlvbiBnZXRDdXJyZW50U2NyaXB0U291cmNlKCkge1xyXG5cdC8vIGBkb2N1bWVudC5jdXJyZW50U2NyaXB0YCBpcyB0aGUgbW9zdCBhY2N1cmF0ZSB3YXkgdG8gZmluZCB0aGUgY3VycmVudCBzY3JpcHQsXHJcblx0Ly8gYnV0IGlzIG5vdCBzdXBwb3J0ZWQgaW4gYWxsIGJyb3dzZXJzLlxyXG5cdGlmKGRvY3VtZW50LmN1cnJlbnRTY3JpcHQpXHJcblx0XHRyZXR1cm4gZG9jdW1lbnQuY3VycmVudFNjcmlwdC5nZXRBdHRyaWJ1dGUoXCJzcmNcIik7XHJcblx0Ly8gRmFsbCBiYWNrIHRvIGdldHRpbmcgYWxsIHNjcmlwdHMgaW4gdGhlIGRvY3VtZW50LlxyXG5cdHZhciBzY3JpcHRFbGVtZW50cyA9IGRvY3VtZW50LnNjcmlwdHMgfHwgW107XHJcblx0dmFyIGN1cnJlbnRTY3JpcHQgPSBzY3JpcHRFbGVtZW50c1tzY3JpcHRFbGVtZW50cy5sZW5ndGggLSAxXTtcclxuXHRpZihjdXJyZW50U2NyaXB0KVxyXG5cdFx0cmV0dXJuIGN1cnJlbnRTY3JpcHQuZ2V0QXR0cmlidXRlKFwic3JjXCIpO1xyXG5cdC8vIEZhaWwgYXMgdGhlcmUgd2FzIG5vIHNjcmlwdCB0byB1c2UuXHJcblx0dGhyb3cgbmV3IEVycm9yKFwiW1dEU10gRmFpbGVkIHRvIGdldCBjdXJyZW50IHNjcmlwdCBzb3VyY2VcIik7XHJcbn1cclxuXHJcbnZhciB1cmxQYXJ0cztcclxuaWYodHlwZW9mIF9fcmVzb3VyY2VRdWVyeSA9PT0gXCJzdHJpbmdcIiAmJiBfX3Jlc291cmNlUXVlcnkpIHtcclxuXHQvLyBJZiB0aGlzIGJ1bmRsZSBpcyBpbmxpbmVkLCB1c2UgdGhlIHJlc291cmNlIHF1ZXJ5IHRvIGdldCB0aGUgY29ycmVjdCB1cmwuXHJcblx0dXJsUGFydHMgPSB1cmwucGFyc2UoX19yZXNvdXJjZVF1ZXJ5LnN1YnN0cigxKSk7XHJcbn0gZWxzZSB7XHJcblx0Ly8gRWxzZSwgZ2V0IHRoZSB1cmwgZnJvbSB0aGUgPHNjcmlwdD4gdGhpcyBmaWxlIHdhcyBjYWxsZWQgd2l0aC5cclxuXHR2YXIgc2NyaXB0SG9zdCA9IGdldEN1cnJlbnRTY3JpcHRTb3VyY2UoKTtcclxuXHRzY3JpcHRIb3N0ID0gc2NyaXB0SG9zdC5yZXBsYWNlKC9cXC9bXlxcL10rJC8sIFwiXCIpO1xyXG5cdHVybFBhcnRzID0gdXJsLnBhcnNlKChzY3JpcHRIb3N0ID8gc2NyaXB0SG9zdCA6IFwiL1wiKSwgZmFsc2UsIHRydWUpO1xyXG59XHJcblxyXG52YXIgaG90ID0gZmFsc2U7XHJcbnZhciBpbml0aWFsID0gdHJ1ZTtcclxudmFyIGN1cnJlbnRIYXNoID0gXCJcIjtcclxudmFyIGxvZ0xldmVsID0gXCJpbmZvXCI7XHJcbnZhciB1c2VXYXJuaW5nT3ZlcmxheSA9IGZhbHNlO1xyXG52YXIgdXNlRXJyb3JPdmVybGF5ID0gZmFsc2U7XHJcblxyXG5mdW5jdGlvbiBsb2cobGV2ZWwsIG1zZykge1xyXG5cdGlmKGxvZ0xldmVsID09PSBcImluZm9cIiAmJiBsZXZlbCA9PT0gXCJpbmZvXCIpXHJcblx0XHRyZXR1cm4gY29uc29sZS5sb2cobXNnKTtcclxuXHRpZihbXCJpbmZvXCIsIFwid2FybmluZ1wiXS5pbmRleE9mKGxvZ0xldmVsKSA+PSAwICYmIGxldmVsID09PSBcIndhcm5pbmdcIilcclxuXHRcdHJldHVybiBjb25zb2xlLndhcm4obXNnKTtcclxuXHRpZihbXCJpbmZvXCIsIFwid2FybmluZ1wiLCBcImVycm9yXCJdLmluZGV4T2YobG9nTGV2ZWwpID49IDAgJiYgbGV2ZWwgPT09IFwiZXJyb3JcIilcclxuXHRcdHJldHVybiBjb25zb2xlLmVycm9yKG1zZyk7XHJcbn1cclxuXHJcbi8vIFNlbmQgbWVzc2FnZXMgdG8gdGhlIG91dHNpZGUsIHNvIHBsdWdpbnMgY2FuIGNvbnN1bWUgaXQuXHJcbmZ1bmN0aW9uIHNlbmRNc2codHlwZSwgZGF0YSkge1xyXG5cdGlmKHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiICYmIHNlbGYud2luZG93KSB7XHJcblx0XHRzZWxmLnBvc3RNZXNzYWdlKHtcclxuXHRcdFx0dHlwZTogXCJ3ZWJwYWNrXCIgKyB0eXBlLFxyXG5cdFx0XHRkYXRhOiBkYXRhXHJcblx0XHR9LCBcIipcIik7XHJcblx0fVxyXG59XHJcblxyXG52YXIgb25Tb2NrZXRNc2cgPSB7XHJcblx0aG90OiBmdW5jdGlvbigpIHtcclxuXHRcdGhvdCA9IHRydWU7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gSG90IE1vZHVsZSBSZXBsYWNlbWVudCBlbmFibGVkLlwiKTtcclxuXHR9LFxyXG5cdGludmFsaWQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCB1cGRhdGVkLiBSZWNvbXBpbGluZy4uLlwiKTtcclxuXHRcdHNlbmRNc2coXCJJbnZhbGlkXCIpO1xyXG5cdH0sXHJcblx0aGFzaDogZnVuY3Rpb24oaGFzaCkge1xyXG5cdFx0Y3VycmVudEhhc2ggPSBoYXNoO1xyXG5cdH0sXHJcblx0XCJzdGlsbC1va1wiOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBOb3RoaW5nIGNoYW5nZWQuXCIpXHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSB8fCB1c2VFcnJvck92ZXJsYXkpIG92ZXJsYXkuY2xlYXIoKTtcclxuXHRcdHNlbmRNc2coXCJTdGlsbE9rXCIpO1xyXG5cdH0sXHJcblx0XCJsb2ctbGV2ZWxcIjogZnVuY3Rpb24obGV2ZWwpIHtcclxuXHRcdGxvZ0xldmVsID0gbGV2ZWw7XHJcblx0fSxcclxuXHRcIm92ZXJsYXlcIjogZnVuY3Rpb24ob3ZlcmxheSkge1xyXG5cdFx0aWYodHlwZW9mIGRvY3VtZW50ICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcblx0XHRcdGlmKHR5cGVvZihvdmVybGF5KSA9PT0gXCJib29sZWFuXCIpIHtcclxuXHRcdFx0XHR1c2VXYXJuaW5nT3ZlcmxheSA9IG92ZXJsYXk7XHJcblx0XHRcdFx0dXNlRXJyb3JPdmVybGF5ID0gb3ZlcmxheTtcclxuXHRcdFx0fSBlbHNlIGlmKG92ZXJsYXkpIHtcclxuXHRcdFx0XHR1c2VXYXJuaW5nT3ZlcmxheSA9IG92ZXJsYXkud2FybmluZ3M7XHJcblx0XHRcdFx0dXNlRXJyb3JPdmVybGF5ID0gb3ZlcmxheS5lcnJvcnM7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9LFxyXG5cdG9rOiBmdW5jdGlvbigpIHtcclxuXHRcdHNlbmRNc2coXCJPa1wiKTtcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5IHx8IHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5jbGVhcigpO1xyXG5cdFx0aWYoaW5pdGlhbCkgcmV0dXJuIGluaXRpYWwgPSBmYWxzZTtcclxuXHRcdHJlbG9hZEFwcCgpO1xyXG5cdH0sXHJcblx0XCJjb250ZW50LWNoYW5nZWRcIjogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQ29udGVudCBiYXNlIGNoYW5nZWQuIFJlbG9hZGluZy4uLlwiKVxyXG5cdFx0c2VsZi5sb2NhdGlvbi5yZWxvYWQoKTtcclxuXHR9LFxyXG5cdHdhcm5pbmdzOiBmdW5jdGlvbih3YXJuaW5ncykge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIFdhcm5pbmdzIHdoaWxlIGNvbXBpbGluZy5cIik7XHJcblx0XHR2YXIgc3RyaXBwZWRXYXJuaW5ncyA9IHdhcm5pbmdzLm1hcChmdW5jdGlvbih3YXJuaW5nKSB7XHJcblx0XHRcdHJldHVybiBzdHJpcEFuc2kod2FybmluZyk7XHJcblx0XHR9KTtcclxuXHRcdHNlbmRNc2coXCJXYXJuaW5nc1wiLCBzdHJpcHBlZFdhcm5pbmdzKTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBzdHJpcHBlZFdhcm5pbmdzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRjb25zb2xlLndhcm4oc3RyaXBwZWRXYXJuaW5nc1tpXSk7XHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSkgb3ZlcmxheS5zaG93TWVzc2FnZSh3YXJuaW5ncyk7XHJcblxyXG5cdFx0aWYoaW5pdGlhbCkgcmV0dXJuIGluaXRpYWwgPSBmYWxzZTtcclxuXHRcdHJlbG9hZEFwcCgpO1xyXG5cdH0sXHJcblx0ZXJyb3JzOiBmdW5jdGlvbihlcnJvcnMpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBFcnJvcnMgd2hpbGUgY29tcGlsaW5nLiBSZWxvYWQgcHJldmVudGVkLlwiKTtcclxuXHRcdHZhciBzdHJpcHBlZEVycm9ycyA9IGVycm9ycy5tYXAoZnVuY3Rpb24oZXJyb3IpIHtcclxuXHRcdFx0cmV0dXJuIHN0cmlwQW5zaShlcnJvcik7XHJcblx0XHR9KTtcclxuXHRcdHNlbmRNc2coXCJFcnJvcnNcIiwgc3RyaXBwZWRFcnJvcnMpO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHN0cmlwcGVkRXJyb3JzLmxlbmd0aDsgaSsrKVxyXG5cdFx0XHRjb25zb2xlLmVycm9yKHN0cmlwcGVkRXJyb3JzW2ldKTtcclxuXHRcdGlmKHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5zaG93TWVzc2FnZShlcnJvcnMpO1xyXG5cdH0sXHJcblx0ZXJyb3I6IGZ1bmN0aW9uKGVycm9yKSB7XHJcblx0XHRjb25zb2xlLmVycm9yKGVycm9yKTtcclxuXHR9LFxyXG5cdGNsb3NlOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImVycm9yXCIsIFwiW1dEU10gRGlzY29ubmVjdGVkIVwiKTtcclxuXHRcdHNlbmRNc2coXCJDbG9zZVwiKTtcclxuXHR9XHJcbn07XHJcblxyXG52YXIgaG9zdG5hbWUgPSB1cmxQYXJ0cy5ob3N0bmFtZTtcclxudmFyIHByb3RvY29sID0gdXJsUGFydHMucHJvdG9jb2w7XHJcblxyXG5cclxuLy9jaGVjayBpcHY0IGFuZCBpcHY2IGBhbGwgaG9zdG5hbWVgXHJcbmlmKGhvc3RuYW1lID09PSBcIjAuMC4wLjBcIiB8fCBob3N0bmFtZSA9PT0gXCI6OlwiKSB7XHJcblx0Ly8gd2h5IGRvIHdlIG5lZWQgdGhpcyBjaGVjaz9cclxuXHQvLyBob3N0bmFtZSBuL2EgZm9yIGZpbGUgcHJvdG9jb2wgKGV4YW1wbGUsIHdoZW4gdXNpbmcgZWxlY3Ryb24sIGlvbmljKVxyXG5cdC8vIHNlZTogaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2svd2VicGFjay1kZXYtc2VydmVyL3B1bGwvMzg0XHJcblx0aWYoc2VsZi5sb2NhdGlvbi5ob3N0bmFtZSAmJiAhIX5zZWxmLmxvY2F0aW9uLnByb3RvY29sLmluZGV4T2YoXCJodHRwXCIpKSB7XHJcblx0XHRob3N0bmFtZSA9IHNlbGYubG9jYXRpb24uaG9zdG5hbWU7XHJcblx0fVxyXG59XHJcblxyXG4vLyBgaG9zdG5hbWVgIGNhbiBiZSBlbXB0eSB3aGVuIHRoZSBzY3JpcHQgcGF0aCBpcyByZWxhdGl2ZS4gSW4gdGhhdCBjYXNlLCBzcGVjaWZ5aW5nXHJcbi8vIGEgcHJvdG9jb2wgd291bGQgcmVzdWx0IGluIGFuIGludmFsaWQgVVJMLlxyXG4vLyBXaGVuIGh0dHBzIGlzIHVzZWQgaW4gdGhlIGFwcCwgc2VjdXJlIHdlYnNvY2tldHMgYXJlIGFsd2F5cyBuZWNlc3NhcnlcclxuLy8gYmVjYXVzZSB0aGUgYnJvd3NlciBkb2Vzbid0IGFjY2VwdCBub24tc2VjdXJlIHdlYnNvY2tldHMuXHJcbmlmKGhvc3RuYW1lICYmIChzZWxmLmxvY2F0aW9uLnByb3RvY29sID09PSBcImh0dHBzOlwiIHx8IHVybFBhcnRzLmhvc3RuYW1lID09PSBcIjAuMC4wLjBcIikpIHtcclxuXHRwcm90b2NvbCA9IHNlbGYubG9jYXRpb24ucHJvdG9jb2w7XHJcbn1cclxuXHJcbnZhciBzb2NrZXRVcmwgPSB1cmwuZm9ybWF0KHtcclxuXHRwcm90b2NvbDogcHJvdG9jb2wsXHJcblx0YXV0aDogdXJsUGFydHMuYXV0aCxcclxuXHRob3N0bmFtZTogaG9zdG5hbWUsXHJcblx0cG9ydDogKHVybFBhcnRzLnBvcnQgPT09IFwiMFwiKSA/IHNlbGYubG9jYXRpb24ucG9ydCA6IHVybFBhcnRzLnBvcnQsXHJcblx0cGF0aG5hbWU6IHVybFBhcnRzLnBhdGggPT0gbnVsbCB8fCB1cmxQYXJ0cy5wYXRoID09PSBcIi9cIiA/IFwiL3NvY2tqcy1ub2RlXCIgOiB1cmxQYXJ0cy5wYXRoXHJcbn0pO1xyXG5cclxuc29ja2V0KHNvY2tldFVybCwgb25Tb2NrZXRNc2cpO1xyXG5cclxudmFyIGlzVW5sb2FkaW5nID0gZmFsc2U7XHJcbnNlbGYuYWRkRXZlbnRMaXN0ZW5lcihcImJlZm9yZXVubG9hZFwiLCBmdW5jdGlvbigpIHtcclxuXHRpc1VubG9hZGluZyA9IHRydWU7XHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gcmVsb2FkQXBwKCkge1xyXG5cdGlmKGlzVW5sb2FkaW5nKSB7XHJcblx0XHRyZXR1cm47XHJcblx0fVxyXG5cdGlmKGhvdCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCBob3QgdXBkYXRlLi4uXCIpO1xyXG5cdFx0dmFyIGhvdEVtaXR0ZXIgPSByZXF1aXJlKFwid2VicGFjay9ob3QvZW1pdHRlclwiKTtcclxuXHRcdGhvdEVtaXR0ZXIuZW1pdChcIndlYnBhY2tIb3RVcGRhdGVcIiwgY3VycmVudEhhc2gpO1xyXG5cdFx0aWYodHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgJiYgc2VsZi53aW5kb3cpIHtcclxuXHRcdFx0Ly8gYnJvYWRjYXN0IHVwZGF0ZSB0byB3aW5kb3dcclxuXHRcdFx0c2VsZi5wb3N0TWVzc2FnZShcIndlYnBhY2tIb3RVcGRhdGVcIiArIGN1cnJlbnRIYXNoLCBcIipcIik7XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBBcHAgdXBkYXRlZC4gUmVsb2FkaW5nLi4uXCIpO1xyXG5cdFx0c2VsZi5sb2NhdGlvbi5yZWxvYWQoKTtcclxuXHR9XHJcbn1cclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MS9cbi8vIG1vZHVsZSBpZCA9IDc2M1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwic291cmNlUm9vdCI6IiJ9