webpackHotUpdate(0,{

/***/ 325:
false,

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _Drawer = __webpack_require__(226);

var _Drawer2 = _interopRequireDefault(_Drawer);

var _MenuItem = __webpack_require__(102);

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _viewList = __webpack_require__(657);

var _viewList2 = _interopRequireDefault(_viewList);

var _noteAdd = __webpack_require__(655);

var _noteAdd2 = _interopRequireDefault(_noteAdd);

var _search = __webpack_require__(656);

var _search2 = _interopRequireDefault(_search);

var _favorite = __webpack_require__(654);

var _favorite2 = _interopRequireDefault(_favorite);

var _styleObj = __webpack_require__(329);

var _styleObj2 = _interopRequireDefault(_styleObj);

var _components = __webpack_require__(778);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AppContainer = function (_React$Component) {
  _inherits(AppContainer, _React$Component);

  function AppContainer() {
    _classCallCheck(this, AppContainer);

    return _possibleConstructorReturn(this, (AppContainer.__proto__ || Object.getPrototypeOf(AppContainer)).apply(this, arguments));
  }

  _createClass(AppContainer, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_components.AsideMenuComponent, null);
    }
  }]);

  return AppContainer;
}(_react2.default.Component);

exports.default = AppContainer;

/***/ }),

/***/ 774:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(775);
__webpack_require__(326);
__webpack_require__(776);
module.exports = __webpack_require__(323);


/***/ }),

/***/ 775:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/* global __resourceQuery */
var url = __webpack_require__(320);
var stripAnsi = __webpack_require__(313);
var socket = __webpack_require__(322);
var overlay = __webpack_require__(321);

function getCurrentScriptSource() {
	// `document.currentScript` is the most accurate way to find the current script,
	// but is not supported in all browsers.
	if(document.currentScript)
		return document.currentScript.getAttribute("src");
	// Fall back to getting all scripts in the document.
	var scriptElements = document.scripts || [];
	var currentScript = scriptElements[scriptElements.length - 1];
	if(currentScript)
		return currentScript.getAttribute("src");
	// Fail as there was no script to use.
	throw new Error("[WDS] Failed to get current script source");
}

var urlParts;
if(true) {
	// If this bundle is inlined, use the resource query to get the correct url.
	urlParts = url.parse(__resourceQuery.substr(1));
} else {
	// Else, get the url from the <script> this file was called with.
	var scriptHost = getCurrentScriptSource();
	scriptHost = scriptHost.replace(/\/[^\/]+$/, "");
	urlParts = url.parse((scriptHost ? scriptHost : "/"), false, true);
}

var hot = false;
var initial = true;
var currentHash = "";
var logLevel = "info";
var useWarningOverlay = false;
var useErrorOverlay = false;

function log(level, msg) {
	if(logLevel === "info" && level === "info")
		return console.log(msg);
	if(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning")
		return console.warn(msg);
	if(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error")
		return console.error(msg);
}

// Send messages to the outside, so plugins can consume it.
function sendMsg(type, data) {
	if(typeof self !== "undefined" && self.window) {
		self.postMessage({
			type: "webpack" + type,
			data: data
		}, "*");
	}
}

var onSocketMsg = {
	hot: function() {
		hot = true;
		log("info", "[WDS] Hot Module Replacement enabled.");
	},
	invalid: function() {
		log("info", "[WDS] App updated. Recompiling...");
		sendMsg("Invalid");
	},
	hash: function(hash) {
		currentHash = hash;
	},
	"still-ok": function() {
		log("info", "[WDS] Nothing changed.")
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		sendMsg("StillOk");
	},
	"log-level": function(level) {
		logLevel = level;
	},
	"overlay": function(overlay) {
		if(typeof document !== "undefined") {
			if(typeof(overlay) === "boolean") {
				useWarningOverlay = overlay;
				useErrorOverlay = overlay;
			} else if(overlay) {
				useWarningOverlay = overlay.warnings;
				useErrorOverlay = overlay.errors;
			}
		}
	},
	ok: function() {
		sendMsg("Ok");
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		if(initial) return initial = false;
		reloadApp();
	},
	"content-changed": function() {
		log("info", "[WDS] Content base changed. Reloading...")
		self.location.reload();
	},
	warnings: function(warnings) {
		log("info", "[WDS] Warnings while compiling.");
		var strippedWarnings = warnings.map(function(warning) {
			return stripAnsi(warning);
		});
		sendMsg("Warnings", strippedWarnings);
		for(var i = 0; i < strippedWarnings.length; i++)
			console.warn(strippedWarnings[i]);
		if(useWarningOverlay) overlay.showMessage(warnings);

		if(initial) return initial = false;
		reloadApp();
	},
	errors: function(errors) {
		log("info", "[WDS] Errors while compiling. Reload prevented.");
		var strippedErrors = errors.map(function(error) {
			return stripAnsi(error);
		});
		sendMsg("Errors", strippedErrors);
		for(var i = 0; i < strippedErrors.length; i++)
			console.error(strippedErrors[i]);
		if(useErrorOverlay) overlay.showMessage(errors);
	},
	error: function(error) {
		console.error(error);
	},
	close: function() {
		log("error", "[WDS] Disconnected!");
		sendMsg("Close");
	}
};

var hostname = urlParts.hostname;
var protocol = urlParts.protocol;


//check ipv4 and ipv6 `all hostname`
if(hostname === "0.0.0.0" || hostname === "::") {
	// why do we need this check?
	// hostname n/a for file protocol (example, when using electron, ionic)
	// see: https://github.com/webpack/webpack-dev-server/pull/384
	if(self.location.hostname && !!~self.location.protocol.indexOf("http")) {
		hostname = self.location.hostname;
	}
}

// `hostname` can be empty when the script path is relative. In that case, specifying
// a protocol would result in an invalid URL.
// When https is used in the app, secure websockets are always necessary
// because the browser doesn't accept non-secure websockets.
if(hostname && (self.location.protocol === "https:" || urlParts.hostname === "0.0.0.0")) {
	protocol = self.location.protocol;
}

var socketUrl = url.format({
	protocol: protocol,
	auth: urlParts.auth,
	hostname: hostname,
	port: (urlParts.port === "0") ? self.location.port : urlParts.port,
	pathname: urlParts.path == null || urlParts.path === "/" ? "/sockjs-node" : urlParts.path
});

socket(socketUrl, onSocketMsg);

var isUnloading = false;
self.addEventListener("beforeunload", function() {
	isUnloading = true;
});

function reloadApp() {
	if(isUnloading) {
		return;
	}
	if(hot) {
		log("info", "[WDS] App hot update...");
		var hotEmitter = __webpack_require__(186);
		hotEmitter.emit("webpackHotUpdate", currentHash);
		if(typeof self !== "undefined" && self.window) {
			// broadcast update to window
			self.postMessage("webpackHotUpdate" + currentHash, "*");
		}
	} else {
		log("info", "[WDS] App updated. Reloading...");
		self.location.reload();
	}
}

/* WEBPACK VAR INJECTION */}.call(exports, "?http://localhost:8081"))

/***/ }),

/***/ 776:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/* global __resourceQuery */
var url = __webpack_require__(320);
var stripAnsi = __webpack_require__(313);
var socket = __webpack_require__(322);
var overlay = __webpack_require__(321);

function getCurrentScriptSource() {
	// `document.currentScript` is the most accurate way to find the current script,
	// but is not supported in all browsers.
	if(document.currentScript)
		return document.currentScript.getAttribute("src");
	// Fall back to getting all scripts in the document.
	var scriptElements = document.scripts || [];
	var currentScript = scriptElements[scriptElements.length - 1];
	if(currentScript)
		return currentScript.getAttribute("src");
	// Fail as there was no script to use.
	throw new Error("[WDS] Failed to get current script source");
}

var urlParts;
if(true) {
	// If this bundle is inlined, use the resource query to get the correct url.
	urlParts = url.parse(__resourceQuery.substr(1));
} else {
	// Else, get the url from the <script> this file was called with.
	var scriptHost = getCurrentScriptSource();
	scriptHost = scriptHost.replace(/\/[^\/]+$/, "");
	urlParts = url.parse((scriptHost ? scriptHost : "/"), false, true);
}

var hot = false;
var initial = true;
var currentHash = "";
var logLevel = "info";
var useWarningOverlay = false;
var useErrorOverlay = false;

function log(level, msg) {
	if(logLevel === "info" && level === "info")
		return console.log(msg);
	if(["info", "warning"].indexOf(logLevel) >= 0 && level === "warning")
		return console.warn(msg);
	if(["info", "warning", "error"].indexOf(logLevel) >= 0 && level === "error")
		return console.error(msg);
}

// Send messages to the outside, so plugins can consume it.
function sendMsg(type, data) {
	if(typeof self !== "undefined" && self.window) {
		self.postMessage({
			type: "webpack" + type,
			data: data
		}, "*");
	}
}

var onSocketMsg = {
	hot: function() {
		hot = true;
		log("info", "[WDS] Hot Module Replacement enabled.");
	},
	invalid: function() {
		log("info", "[WDS] App updated. Recompiling...");
		sendMsg("Invalid");
	},
	hash: function(hash) {
		currentHash = hash;
	},
	"still-ok": function() {
		log("info", "[WDS] Nothing changed.")
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		sendMsg("StillOk");
	},
	"log-level": function(level) {
		logLevel = level;
	},
	"overlay": function(overlay) {
		if(typeof document !== "undefined") {
			if(typeof(overlay) === "boolean") {
				useWarningOverlay = overlay;
				useErrorOverlay = overlay;
			} else if(overlay) {
				useWarningOverlay = overlay.warnings;
				useErrorOverlay = overlay.errors;
			}
		}
	},
	ok: function() {
		sendMsg("Ok");
		if(useWarningOverlay || useErrorOverlay) overlay.clear();
		if(initial) return initial = false;
		reloadApp();
	},
	"content-changed": function() {
		log("info", "[WDS] Content base changed. Reloading...")
		self.location.reload();
	},
	warnings: function(warnings) {
		log("info", "[WDS] Warnings while compiling.");
		var strippedWarnings = warnings.map(function(warning) {
			return stripAnsi(warning);
		});
		sendMsg("Warnings", strippedWarnings);
		for(var i = 0; i < strippedWarnings.length; i++)
			console.warn(strippedWarnings[i]);
		if(useWarningOverlay) overlay.showMessage(warnings);

		if(initial) return initial = false;
		reloadApp();
	},
	errors: function(errors) {
		log("info", "[WDS] Errors while compiling. Reload prevented.");
		var strippedErrors = errors.map(function(error) {
			return stripAnsi(error);
		});
		sendMsg("Errors", strippedErrors);
		for(var i = 0; i < strippedErrors.length; i++)
			console.error(strippedErrors[i]);
		if(useErrorOverlay) overlay.showMessage(errors);
	},
	error: function(error) {
		console.error(error);
	},
	close: function() {
		log("error", "[WDS] Disconnected!");
		sendMsg("Close");
	}
};

var hostname = urlParts.hostname;
var protocol = urlParts.protocol;


//check ipv4 and ipv6 `all hostname`
if(hostname === "0.0.0.0" || hostname === "::") {
	// why do we need this check?
	// hostname n/a for file protocol (example, when using electron, ionic)
	// see: https://github.com/webpack/webpack-dev-server/pull/384
	if(self.location.hostname && !!~self.location.protocol.indexOf("http")) {
		hostname = self.location.hostname;
	}
}

// `hostname` can be empty when the script path is relative. In that case, specifying
// a protocol would result in an invalid URL.
// When https is used in the app, secure websockets are always necessary
// because the browser doesn't accept non-secure websockets.
if(hostname && (self.location.protocol === "https:" || urlParts.hostname === "0.0.0.0")) {
	protocol = self.location.protocol;
}

var socketUrl = url.format({
	protocol: protocol,
	auth: urlParts.auth,
	hostname: hostname,
	port: (urlParts.port === "0") ? self.location.port : urlParts.port,
	pathname: urlParts.path == null || urlParts.path === "/" ? "/sockjs-node" : urlParts.path
});

socket(socketUrl, onSocketMsg);

var isUnloading = false;
self.addEventListener("beforeunload", function() {
	isUnloading = true;
});

function reloadApp() {
	if(isUnloading) {
		return;
	}
	if(hot) {
		log("info", "[WDS] App hot update...");
		var hotEmitter = __webpack_require__(186);
		hotEmitter.emit("webpackHotUpdate", currentHash);
		if(typeof self !== "undefined" && self.window) {
			// broadcast update to window
			self.postMessage("webpackHotUpdate" + currentHash, "*");
		}
	} else {
		log("info", "[WDS] App updated. Reloading...");
		self.location.reload();
	}
}

/* WEBPACK VAR INJECTION */}.call(exports, "?http://localhost:8081/"))

/***/ }),

/***/ 777:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _Drawer = __webpack_require__(226);

var _Drawer2 = _interopRequireDefault(_Drawer);

var _MenuItem = __webpack_require__(102);

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _viewList = __webpack_require__(657);

var _viewList2 = _interopRequireDefault(_viewList);

var _noteAdd = __webpack_require__(655);

var _noteAdd2 = _interopRequireDefault(_noteAdd);

var _search = __webpack_require__(656);

var _search2 = _interopRequireDefault(_search);

var _favorite = __webpack_require__(654);

var _favorite2 = _interopRequireDefault(_favorite);

var _styleObj = __webpack_require__(329);

var _styleObj2 = _interopRequireDefault(_styleObj);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AsideMenuComponent = function (_React$Component) {
	_inherits(AsideMenuComponent, _React$Component);

	function AsideMenuComponent() {
		_classCallCheck(this, AsideMenuComponent);

		return _possibleConstructorReturn(this, (AsideMenuComponent.__proto__ || Object.getPrototypeOf(AsideMenuComponent)).apply(this, arguments));
	}

	_createClass(AsideMenuComponent, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'main-wrapper' },
				_react2.default.createElement(
					'div',
					{ className: 'aside-wrapper' },
					_react2.default.createElement(
						_Drawer2.default,
						{ open: true, width: 75 },
						_react2.default.createElement(
							_MenuItem2.default,
							{ style: _styleObj2.default.listItem },
							_react2.default.createElement(_viewList2.default, null)
						),
						_react2.default.createElement(
							_MenuItem2.default,
							{ style: _styleObj2.default.listItem },
							_react2.default.createElement(_noteAdd2.default, null)
						),
						_react2.default.createElement(
							_MenuItem2.default,
							{ style: _styleObj2.default.listItem },
							_react2.default.createElement(_search2.default, null)
						),
						_react2.default.createElement(
							_MenuItem2.default,
							{ style: _styleObj2.default.listItem },
							_react2.default.createElement(_favorite2.default, null)
						)
					)
				)
			);
		}
	}]);

	return AsideMenuComponent;
}(_react2.default.Component);

exports.default = AsideMenuComponent;

/***/ }),

/***/ 778:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AsideMenuComponent = undefined;

var _AsideMenuComponent2 = __webpack_require__(777);

var _AsideMenuComponent3 = _interopRequireDefault(_AsideMenuComponent2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.AsideMenuComponent = _AsideMenuComponent3.default;

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY29udGFpbmVycy9BcHBDb250YWluZXIuanM/NDIwNSIsIndlYnBhY2s6Ly8vKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50P2MwYTAiLCJ3ZWJwYWNrOi8vLyh3ZWJwYWNrKS1kZXYtc2VydmVyL2NsaWVudD8yM2RhKiIsIndlYnBhY2s6Ly8vKHdlYnBhY2spLWRldi1zZXJ2ZXIvY2xpZW50PzhiZTcqIiwid2VicGFjazovLy8od2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/ZWMyYyoiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvQXNpZGVNZW51Q29tcG9uZW50LmpzPzE2MWMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvaW5kZXguanM/NmM2NiJdLCJuYW1lcyI6WyJBcHBDb250YWluZXIiLCJDb21wb25lbnQiLCJBc2lkZU1lbnVDb21wb25lbnQiLCJsaXN0SXRlbSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7SUFFTUEsWTs7Ozs7Ozs7Ozs7NkJBQ0s7QUFDUCxhQUNDLG1FQUREO0FBR0Q7Ozs7RUFMd0IsZ0JBQU1DLFM7O2tCQVFsQkQsWTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJmLHVEQ0FBO0FBQ0EsVURBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxnQkRBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxhREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBLGNEQUEsbUJDQUEsQ0RBQSxHQ0FBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsR0RBQSxJQ0FBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGdCQUFnQiw2QkFBNkI7QUFDN0M7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsZ0JBQWdCLDJCQUEyQjtBQUMzQztBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CREFBLG1CQ0FBLENEQUEsR0NBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQ3ZMQSx1RENBQTtBQUNBLFVEQUEsbUJDQUEsQ0RBQSxHQ0FBO0FBQ0EsZ0JEQUEsbUJDQUEsQ0RBQSxHQ0FBO0FBQ0EsYURBQSxtQkNBQSxDREFBLEdDQUE7QUFDQSxjREFBLG1CQ0FBLENEQUEsR0NBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdEQUEsSUNBQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxnQkFBZ0IsNkJBQTZCO0FBQzdDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGdCQUFnQiwyQkFBMkI7QUFDM0M7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkRBQSxtQkNBQSxDREFBLEdDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkxBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7OztJQUVxQkUsa0I7Ozs7Ozs7Ozs7OzJCQUNYO0FBQ1IsVUFDQztBQUFBO0FBQUEsTUFBSyxXQUFVLGNBQWY7QUFDQztBQUFBO0FBQUEsT0FBSyxXQUFVLGVBQWY7QUFDQztBQUFBO0FBQUEsUUFBUSxNQUFNLElBQWQsRUFBb0IsT0FBTyxFQUEzQjtBQUNDO0FBQUE7QUFBQSxTQUFVLE9BQU8sbUJBQU1DLFFBQXZCO0FBQWlDO0FBQWpDLE9BREQ7QUFFQztBQUFBO0FBQUEsU0FBVSxPQUFPLG1CQUFNQSxRQUF2QjtBQUFpQztBQUFqQyxPQUZEO0FBR0M7QUFBQTtBQUFBLFNBQVUsT0FBTyxtQkFBTUEsUUFBdkI7QUFBaUM7QUFBakMsT0FIRDtBQUlDO0FBQUE7QUFBQSxTQUFVLE9BQU8sbUJBQU1BLFFBQXZCO0FBQWlDO0FBQWpDO0FBSkQ7QUFERDtBQURELElBREQ7QUFZQTs7OztFQWQ4QyxnQkFBTUYsUzs7a0JBQWpDQyxrQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDVGRBLGtCIiwiZmlsZSI6IjAuYTRmYTBlZDFhNDM5NDI3MzI0MDcuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgRHJhd2VyIGZyb20gJ21hdGVyaWFsLXVpL0RyYXdlcic7XG5pbXBvcnQgTWVudUl0ZW0gZnJvbSAnbWF0ZXJpYWwtdWkvTWVudUl0ZW0nO1xuaW1wb3J0IExpc3RJY29uIGZyb20gJ3JlYWN0LW1hdGVyaWFsLWljb25zL2ljb25zL2FjdGlvbi92aWV3LWxpc3QnO1xuaW1wb3J0IEFkZEljb24gZnJvbSAncmVhY3QtbWF0ZXJpYWwtaWNvbnMvaWNvbnMvYWN0aW9uL25vdGUtYWRkJztcbmltcG9ydCBTZWFyY2hJY29uIGZyb20gJ3JlYWN0LW1hdGVyaWFsLWljb25zL2ljb25zL2FjdGlvbi9zZWFyY2gnO1xuaW1wb3J0IEZhdm9yaXRlSWNvbiBmcm9tICdyZWFjdC1tYXRlcmlhbC1pY29ucy9pY29ucy9hY3Rpb24vZmF2b3JpdGUnO1xuaW1wb3J0IHN0eWxlIGZyb20gJy4uL2Fzc2V0cy9zdHlsZU9iaic7XG5pbXBvcnQgeyBBc2lkZU1lbnVDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzJztcblxuY2xhc3MgQXBwQ29udGFpbmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG5cdCAgICA8QXNpZGVNZW51Q29tcG9uZW50IC8+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBBcHBDb250YWluZXI7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY29udGFpbmVycy9BcHBDb250YWluZXIuanMiLCJudWxsXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vICh3ZWJwYWNrKS1kZXYtc2VydmVyL2NsaWVudD9odHRwOi9sb2NhbGhvc3Q6ODA4MSIsIi8qIGdsb2JhbCBfX3Jlc291cmNlUXVlcnkgKi9cclxudmFyIHVybCA9IHJlcXVpcmUoXCJ1cmxcIik7XHJcbnZhciBzdHJpcEFuc2kgPSByZXF1aXJlKFwic3RyaXAtYW5zaVwiKTtcclxudmFyIHNvY2tldCA9IHJlcXVpcmUoXCIuL3NvY2tldFwiKTtcclxudmFyIG92ZXJsYXkgPSByZXF1aXJlKFwiLi9vdmVybGF5XCIpO1xyXG5cclxuZnVuY3Rpb24gZ2V0Q3VycmVudFNjcmlwdFNvdXJjZSgpIHtcclxuXHQvLyBgZG9jdW1lbnQuY3VycmVudFNjcmlwdGAgaXMgdGhlIG1vc3QgYWNjdXJhdGUgd2F5IHRvIGZpbmQgdGhlIGN1cnJlbnQgc2NyaXB0LFxyXG5cdC8vIGJ1dCBpcyBub3Qgc3VwcG9ydGVkIGluIGFsbCBicm93c2Vycy5cclxuXHRpZihkb2N1bWVudC5jdXJyZW50U2NyaXB0KVxyXG5cdFx0cmV0dXJuIGRvY3VtZW50LmN1cnJlbnRTY3JpcHQuZ2V0QXR0cmlidXRlKFwic3JjXCIpO1xyXG5cdC8vIEZhbGwgYmFjayB0byBnZXR0aW5nIGFsbCBzY3JpcHRzIGluIHRoZSBkb2N1bWVudC5cclxuXHR2YXIgc2NyaXB0RWxlbWVudHMgPSBkb2N1bWVudC5zY3JpcHRzIHx8IFtdO1xyXG5cdHZhciBjdXJyZW50U2NyaXB0ID0gc2NyaXB0RWxlbWVudHNbc2NyaXB0RWxlbWVudHMubGVuZ3RoIC0gMV07XHJcblx0aWYoY3VycmVudFNjcmlwdClcclxuXHRcdHJldHVybiBjdXJyZW50U2NyaXB0LmdldEF0dHJpYnV0ZShcInNyY1wiKTtcclxuXHQvLyBGYWlsIGFzIHRoZXJlIHdhcyBubyBzY3JpcHQgdG8gdXNlLlxyXG5cdHRocm93IG5ldyBFcnJvcihcIltXRFNdIEZhaWxlZCB0byBnZXQgY3VycmVudCBzY3JpcHQgc291cmNlXCIpO1xyXG59XHJcblxyXG52YXIgdXJsUGFydHM7XHJcbmlmKHR5cGVvZiBfX3Jlc291cmNlUXVlcnkgPT09IFwic3RyaW5nXCIgJiYgX19yZXNvdXJjZVF1ZXJ5KSB7XHJcblx0Ly8gSWYgdGhpcyBidW5kbGUgaXMgaW5saW5lZCwgdXNlIHRoZSByZXNvdXJjZSBxdWVyeSB0byBnZXQgdGhlIGNvcnJlY3QgdXJsLlxyXG5cdHVybFBhcnRzID0gdXJsLnBhcnNlKF9fcmVzb3VyY2VRdWVyeS5zdWJzdHIoMSkpO1xyXG59IGVsc2Uge1xyXG5cdC8vIEVsc2UsIGdldCB0aGUgdXJsIGZyb20gdGhlIDxzY3JpcHQ+IHRoaXMgZmlsZSB3YXMgY2FsbGVkIHdpdGguXHJcblx0dmFyIHNjcmlwdEhvc3QgPSBnZXRDdXJyZW50U2NyaXB0U291cmNlKCk7XHJcblx0c2NyaXB0SG9zdCA9IHNjcmlwdEhvc3QucmVwbGFjZSgvXFwvW15cXC9dKyQvLCBcIlwiKTtcclxuXHR1cmxQYXJ0cyA9IHVybC5wYXJzZSgoc2NyaXB0SG9zdCA/IHNjcmlwdEhvc3QgOiBcIi9cIiksIGZhbHNlLCB0cnVlKTtcclxufVxyXG5cclxudmFyIGhvdCA9IGZhbHNlO1xyXG52YXIgaW5pdGlhbCA9IHRydWU7XHJcbnZhciBjdXJyZW50SGFzaCA9IFwiXCI7XHJcbnZhciBsb2dMZXZlbCA9IFwiaW5mb1wiO1xyXG52YXIgdXNlV2FybmluZ092ZXJsYXkgPSBmYWxzZTtcclxudmFyIHVzZUVycm9yT3ZlcmxheSA9IGZhbHNlO1xyXG5cclxuZnVuY3Rpb24gbG9nKGxldmVsLCBtc2cpIHtcclxuXHRpZihsb2dMZXZlbCA9PT0gXCJpbmZvXCIgJiYgbGV2ZWwgPT09IFwiaW5mb1wiKVxyXG5cdFx0cmV0dXJuIGNvbnNvbGUubG9nKG1zZyk7XHJcblx0aWYoW1wiaW5mb1wiLCBcIndhcm5pbmdcIl0uaW5kZXhPZihsb2dMZXZlbCkgPj0gMCAmJiBsZXZlbCA9PT0gXCJ3YXJuaW5nXCIpXHJcblx0XHRyZXR1cm4gY29uc29sZS53YXJuKG1zZyk7XHJcblx0aWYoW1wiaW5mb1wiLCBcIndhcm5pbmdcIiwgXCJlcnJvclwiXS5pbmRleE9mKGxvZ0xldmVsKSA+PSAwICYmIGxldmVsID09PSBcImVycm9yXCIpXHJcblx0XHRyZXR1cm4gY29uc29sZS5lcnJvcihtc2cpO1xyXG59XHJcblxyXG4vLyBTZW5kIG1lc3NhZ2VzIHRvIHRoZSBvdXRzaWRlLCBzbyBwbHVnaW5zIGNhbiBjb25zdW1lIGl0LlxyXG5mdW5jdGlvbiBzZW5kTXNnKHR5cGUsIGRhdGEpIHtcclxuXHRpZih0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiAmJiBzZWxmLndpbmRvdykge1xyXG5cdFx0c2VsZi5wb3N0TWVzc2FnZSh7XHJcblx0XHRcdHR5cGU6IFwid2VicGFja1wiICsgdHlwZSxcclxuXHRcdFx0ZGF0YTogZGF0YVxyXG5cdFx0fSwgXCIqXCIpO1xyXG5cdH1cclxufVxyXG5cclxudmFyIG9uU29ja2V0TXNnID0ge1xyXG5cdGhvdDogZnVuY3Rpb24oKSB7XHJcblx0XHRob3QgPSB0cnVlO1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnQgZW5hYmxlZC5cIik7XHJcblx0fSxcclxuXHRpbnZhbGlkOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBBcHAgdXBkYXRlZC4gUmVjb21waWxpbmcuLi5cIik7XHJcblx0XHRzZW5kTXNnKFwiSW52YWxpZFwiKTtcclxuXHR9LFxyXG5cdGhhc2g6IGZ1bmN0aW9uKGhhc2gpIHtcclxuXHRcdGN1cnJlbnRIYXNoID0gaGFzaDtcclxuXHR9LFxyXG5cdFwic3RpbGwtb2tcIjogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gTm90aGluZyBjaGFuZ2VkLlwiKVxyXG5cdFx0aWYodXNlV2FybmluZ092ZXJsYXkgfHwgdXNlRXJyb3JPdmVybGF5KSBvdmVybGF5LmNsZWFyKCk7XHJcblx0XHRzZW5kTXNnKFwiU3RpbGxPa1wiKTtcclxuXHR9LFxyXG5cdFwibG9nLWxldmVsXCI6IGZ1bmN0aW9uKGxldmVsKSB7XHJcblx0XHRsb2dMZXZlbCA9IGxldmVsO1xyXG5cdH0sXHJcblx0XCJvdmVybGF5XCI6IGZ1bmN0aW9uKG92ZXJsYXkpIHtcclxuXHRcdGlmKHR5cGVvZiBkb2N1bWVudCAhPT0gXCJ1bmRlZmluZWRcIikge1xyXG5cdFx0XHRpZih0eXBlb2Yob3ZlcmxheSkgPT09IFwiYm9vbGVhblwiKSB7XHJcblx0XHRcdFx0dXNlV2FybmluZ092ZXJsYXkgPSBvdmVybGF5O1xyXG5cdFx0XHRcdHVzZUVycm9yT3ZlcmxheSA9IG92ZXJsYXk7XHJcblx0XHRcdH0gZWxzZSBpZihvdmVybGF5KSB7XHJcblx0XHRcdFx0dXNlV2FybmluZ092ZXJsYXkgPSBvdmVybGF5Lndhcm5pbmdzO1xyXG5cdFx0XHRcdHVzZUVycm9yT3ZlcmxheSA9IG92ZXJsYXkuZXJyb3JzO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSxcclxuXHRvazogZnVuY3Rpb24oKSB7XHJcblx0XHRzZW5kTXNnKFwiT2tcIik7XHJcblx0XHRpZih1c2VXYXJuaW5nT3ZlcmxheSB8fCB1c2VFcnJvck92ZXJsYXkpIG92ZXJsYXkuY2xlYXIoKTtcclxuXHRcdGlmKGluaXRpYWwpIHJldHVybiBpbml0aWFsID0gZmFsc2U7XHJcblx0XHRyZWxvYWRBcHAoKTtcclxuXHR9LFxyXG5cdFwiY29udGVudC1jaGFuZ2VkXCI6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIENvbnRlbnQgYmFzZSBjaGFuZ2VkLiBSZWxvYWRpbmcuLi5cIilcclxuXHRcdHNlbGYubG9jYXRpb24ucmVsb2FkKCk7XHJcblx0fSxcclxuXHR3YXJuaW5nczogZnVuY3Rpb24od2FybmluZ3MpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBXYXJuaW5ncyB3aGlsZSBjb21waWxpbmcuXCIpO1xyXG5cdFx0dmFyIHN0cmlwcGVkV2FybmluZ3MgPSB3YXJuaW5ncy5tYXAoZnVuY3Rpb24od2FybmluZykge1xyXG5cdFx0XHRyZXR1cm4gc3RyaXBBbnNpKHdhcm5pbmcpO1xyXG5cdFx0fSk7XHJcblx0XHRzZW5kTXNnKFwiV2FybmluZ3NcIiwgc3RyaXBwZWRXYXJuaW5ncyk7XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgc3RyaXBwZWRXYXJuaW5ncy5sZW5ndGg7IGkrKylcclxuXHRcdFx0Y29uc29sZS53YXJuKHN0cmlwcGVkV2FybmluZ3NbaV0pO1xyXG5cdFx0aWYodXNlV2FybmluZ092ZXJsYXkpIG92ZXJsYXkuc2hvd01lc3NhZ2Uod2FybmluZ3MpO1xyXG5cclxuXHRcdGlmKGluaXRpYWwpIHJldHVybiBpbml0aWFsID0gZmFsc2U7XHJcblx0XHRyZWxvYWRBcHAoKTtcclxuXHR9LFxyXG5cdGVycm9yczogZnVuY3Rpb24oZXJyb3JzKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gRXJyb3JzIHdoaWxlIGNvbXBpbGluZy4gUmVsb2FkIHByZXZlbnRlZC5cIik7XHJcblx0XHR2YXIgc3RyaXBwZWRFcnJvcnMgPSBlcnJvcnMubWFwKGZ1bmN0aW9uKGVycm9yKSB7XHJcblx0XHRcdHJldHVybiBzdHJpcEFuc2koZXJyb3IpO1xyXG5cdFx0fSk7XHJcblx0XHRzZW5kTXNnKFwiRXJyb3JzXCIsIHN0cmlwcGVkRXJyb3JzKTtcclxuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBzdHJpcHBlZEVycm9ycy5sZW5ndGg7IGkrKylcclxuXHRcdFx0Y29uc29sZS5lcnJvcihzdHJpcHBlZEVycm9yc1tpXSk7XHJcblx0XHRpZih1c2VFcnJvck92ZXJsYXkpIG92ZXJsYXkuc2hvd01lc3NhZ2UoZXJyb3JzKTtcclxuXHR9LFxyXG5cdGVycm9yOiBmdW5jdGlvbihlcnJvcikge1xyXG5cdFx0Y29uc29sZS5lcnJvcihlcnJvcik7XHJcblx0fSxcclxuXHRjbG9zZTogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJlcnJvclwiLCBcIltXRFNdIERpc2Nvbm5lY3RlZCFcIik7XHJcblx0XHRzZW5kTXNnKFwiQ2xvc2VcIik7XHJcblx0fVxyXG59O1xyXG5cclxudmFyIGhvc3RuYW1lID0gdXJsUGFydHMuaG9zdG5hbWU7XHJcbnZhciBwcm90b2NvbCA9IHVybFBhcnRzLnByb3RvY29sO1xyXG5cclxuXHJcbi8vY2hlY2sgaXB2NCBhbmQgaXB2NiBgYWxsIGhvc3RuYW1lYFxyXG5pZihob3N0bmFtZSA9PT0gXCIwLjAuMC4wXCIgfHwgaG9zdG5hbWUgPT09IFwiOjpcIikge1xyXG5cdC8vIHdoeSBkbyB3ZSBuZWVkIHRoaXMgY2hlY2s/XHJcblx0Ly8gaG9zdG5hbWUgbi9hIGZvciBmaWxlIHByb3RvY29sIChleGFtcGxlLCB3aGVuIHVzaW5nIGVsZWN0cm9uLCBpb25pYylcclxuXHQvLyBzZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS93ZWJwYWNrL3dlYnBhY2stZGV2LXNlcnZlci9wdWxsLzM4NFxyXG5cdGlmKHNlbGYubG9jYXRpb24uaG9zdG5hbWUgJiYgISF+c2VsZi5sb2NhdGlvbi5wcm90b2NvbC5pbmRleE9mKFwiaHR0cFwiKSkge1xyXG5cdFx0aG9zdG5hbWUgPSBzZWxmLmxvY2F0aW9uLmhvc3RuYW1lO1xyXG5cdH1cclxufVxyXG5cclxuLy8gYGhvc3RuYW1lYCBjYW4gYmUgZW1wdHkgd2hlbiB0aGUgc2NyaXB0IHBhdGggaXMgcmVsYXRpdmUuIEluIHRoYXQgY2FzZSwgc3BlY2lmeWluZ1xyXG4vLyBhIHByb3RvY29sIHdvdWxkIHJlc3VsdCBpbiBhbiBpbnZhbGlkIFVSTC5cclxuLy8gV2hlbiBodHRwcyBpcyB1c2VkIGluIHRoZSBhcHAsIHNlY3VyZSB3ZWJzb2NrZXRzIGFyZSBhbHdheXMgbmVjZXNzYXJ5XHJcbi8vIGJlY2F1c2UgdGhlIGJyb3dzZXIgZG9lc24ndCBhY2NlcHQgbm9uLXNlY3VyZSB3ZWJzb2NrZXRzLlxyXG5pZihob3N0bmFtZSAmJiAoc2VsZi5sb2NhdGlvbi5wcm90b2NvbCA9PT0gXCJodHRwczpcIiB8fCB1cmxQYXJ0cy5ob3N0bmFtZSA9PT0gXCIwLjAuMC4wXCIpKSB7XHJcblx0cHJvdG9jb2wgPSBzZWxmLmxvY2F0aW9uLnByb3RvY29sO1xyXG59XHJcblxyXG52YXIgc29ja2V0VXJsID0gdXJsLmZvcm1hdCh7XHJcblx0cHJvdG9jb2w6IHByb3RvY29sLFxyXG5cdGF1dGg6IHVybFBhcnRzLmF1dGgsXHJcblx0aG9zdG5hbWU6IGhvc3RuYW1lLFxyXG5cdHBvcnQ6ICh1cmxQYXJ0cy5wb3J0ID09PSBcIjBcIikgPyBzZWxmLmxvY2F0aW9uLnBvcnQgOiB1cmxQYXJ0cy5wb3J0LFxyXG5cdHBhdGhuYW1lOiB1cmxQYXJ0cy5wYXRoID09IG51bGwgfHwgdXJsUGFydHMucGF0aCA9PT0gXCIvXCIgPyBcIi9zb2NranMtbm9kZVwiIDogdXJsUGFydHMucGF0aFxyXG59KTtcclxuXHJcbnNvY2tldChzb2NrZXRVcmwsIG9uU29ja2V0TXNnKTtcclxuXHJcbnZhciBpc1VubG9hZGluZyA9IGZhbHNlO1xyXG5zZWxmLmFkZEV2ZW50TGlzdGVuZXIoXCJiZWZvcmV1bmxvYWRcIiwgZnVuY3Rpb24oKSB7XHJcblx0aXNVbmxvYWRpbmcgPSB0cnVlO1xyXG59KTtcclxuXHJcbmZ1bmN0aW9uIHJlbG9hZEFwcCgpIHtcclxuXHRpZihpc1VubG9hZGluZykge1xyXG5cdFx0cmV0dXJuO1xyXG5cdH1cclxuXHRpZihob3QpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBBcHAgaG90IHVwZGF0ZS4uLlwiKTtcclxuXHRcdHZhciBob3RFbWl0dGVyID0gcmVxdWlyZShcIndlYnBhY2svaG90L2VtaXR0ZXJcIik7XHJcblx0XHRob3RFbWl0dGVyLmVtaXQoXCJ3ZWJwYWNrSG90VXBkYXRlXCIsIGN1cnJlbnRIYXNoKTtcclxuXHRcdGlmKHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiICYmIHNlbGYud2luZG93KSB7XHJcblx0XHRcdC8vIGJyb2FkY2FzdCB1cGRhdGUgdG8gd2luZG93XHJcblx0XHRcdHNlbGYucG9zdE1lc3NhZ2UoXCJ3ZWJwYWNrSG90VXBkYXRlXCIgKyBjdXJyZW50SGFzaCwgXCIqXCIpO1xyXG5cdFx0fVxyXG5cdH0gZWxzZSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQXBwIHVwZGF0ZWQuIFJlbG9hZGluZy4uLlwiKTtcclxuXHRcdHNlbGYubG9jYXRpb24ucmVsb2FkKCk7XHJcblx0fVxyXG59XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vICh3ZWJwYWNrKS1kZXYtc2VydmVyL2NsaWVudD9odHRwOi8vbG9jYWxob3N0OjgwODFcbi8vIG1vZHVsZSBpZCA9IDc3NVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJudWxsXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vICh3ZWJwYWNrKS1kZXYtc2VydmVyL2NsaWVudD9odHRwOi9sb2NhbGhvc3Q6ODA4MS8iLCIvKiBnbG9iYWwgX19yZXNvdXJjZVF1ZXJ5ICovXHJcbnZhciB1cmwgPSByZXF1aXJlKFwidXJsXCIpO1xyXG52YXIgc3RyaXBBbnNpID0gcmVxdWlyZShcInN0cmlwLWFuc2lcIik7XHJcbnZhciBzb2NrZXQgPSByZXF1aXJlKFwiLi9zb2NrZXRcIik7XHJcbnZhciBvdmVybGF5ID0gcmVxdWlyZShcIi4vb3ZlcmxheVwiKTtcclxuXHJcbmZ1bmN0aW9uIGdldEN1cnJlbnRTY3JpcHRTb3VyY2UoKSB7XHJcblx0Ly8gYGRvY3VtZW50LmN1cnJlbnRTY3JpcHRgIGlzIHRoZSBtb3N0IGFjY3VyYXRlIHdheSB0byBmaW5kIHRoZSBjdXJyZW50IHNjcmlwdCxcclxuXHQvLyBidXQgaXMgbm90IHN1cHBvcnRlZCBpbiBhbGwgYnJvd3NlcnMuXHJcblx0aWYoZG9jdW1lbnQuY3VycmVudFNjcmlwdClcclxuXHRcdHJldHVybiBkb2N1bWVudC5jdXJyZW50U2NyaXB0LmdldEF0dHJpYnV0ZShcInNyY1wiKTtcclxuXHQvLyBGYWxsIGJhY2sgdG8gZ2V0dGluZyBhbGwgc2NyaXB0cyBpbiB0aGUgZG9jdW1lbnQuXHJcblx0dmFyIHNjcmlwdEVsZW1lbnRzID0gZG9jdW1lbnQuc2NyaXB0cyB8fCBbXTtcclxuXHR2YXIgY3VycmVudFNjcmlwdCA9IHNjcmlwdEVsZW1lbnRzW3NjcmlwdEVsZW1lbnRzLmxlbmd0aCAtIDFdO1xyXG5cdGlmKGN1cnJlbnRTY3JpcHQpXHJcblx0XHRyZXR1cm4gY3VycmVudFNjcmlwdC5nZXRBdHRyaWJ1dGUoXCJzcmNcIik7XHJcblx0Ly8gRmFpbCBhcyB0aGVyZSB3YXMgbm8gc2NyaXB0IHRvIHVzZS5cclxuXHR0aHJvdyBuZXcgRXJyb3IoXCJbV0RTXSBGYWlsZWQgdG8gZ2V0IGN1cnJlbnQgc2NyaXB0IHNvdXJjZVwiKTtcclxufVxyXG5cclxudmFyIHVybFBhcnRzO1xyXG5pZih0eXBlb2YgX19yZXNvdXJjZVF1ZXJ5ID09PSBcInN0cmluZ1wiICYmIF9fcmVzb3VyY2VRdWVyeSkge1xyXG5cdC8vIElmIHRoaXMgYnVuZGxlIGlzIGlubGluZWQsIHVzZSB0aGUgcmVzb3VyY2UgcXVlcnkgdG8gZ2V0IHRoZSBjb3JyZWN0IHVybC5cclxuXHR1cmxQYXJ0cyA9IHVybC5wYXJzZShfX3Jlc291cmNlUXVlcnkuc3Vic3RyKDEpKTtcclxufSBlbHNlIHtcclxuXHQvLyBFbHNlLCBnZXQgdGhlIHVybCBmcm9tIHRoZSA8c2NyaXB0PiB0aGlzIGZpbGUgd2FzIGNhbGxlZCB3aXRoLlxyXG5cdHZhciBzY3JpcHRIb3N0ID0gZ2V0Q3VycmVudFNjcmlwdFNvdXJjZSgpO1xyXG5cdHNjcmlwdEhvc3QgPSBzY3JpcHRIb3N0LnJlcGxhY2UoL1xcL1teXFwvXSskLywgXCJcIik7XHJcblx0dXJsUGFydHMgPSB1cmwucGFyc2UoKHNjcmlwdEhvc3QgPyBzY3JpcHRIb3N0IDogXCIvXCIpLCBmYWxzZSwgdHJ1ZSk7XHJcbn1cclxuXHJcbnZhciBob3QgPSBmYWxzZTtcclxudmFyIGluaXRpYWwgPSB0cnVlO1xyXG52YXIgY3VycmVudEhhc2ggPSBcIlwiO1xyXG52YXIgbG9nTGV2ZWwgPSBcImluZm9cIjtcclxudmFyIHVzZVdhcm5pbmdPdmVybGF5ID0gZmFsc2U7XHJcbnZhciB1c2VFcnJvck92ZXJsYXkgPSBmYWxzZTtcclxuXHJcbmZ1bmN0aW9uIGxvZyhsZXZlbCwgbXNnKSB7XHJcblx0aWYobG9nTGV2ZWwgPT09IFwiaW5mb1wiICYmIGxldmVsID09PSBcImluZm9cIilcclxuXHRcdHJldHVybiBjb25zb2xlLmxvZyhtc2cpO1xyXG5cdGlmKFtcImluZm9cIiwgXCJ3YXJuaW5nXCJdLmluZGV4T2YobG9nTGV2ZWwpID49IDAgJiYgbGV2ZWwgPT09IFwid2FybmluZ1wiKVxyXG5cdFx0cmV0dXJuIGNvbnNvbGUud2Fybihtc2cpO1xyXG5cdGlmKFtcImluZm9cIiwgXCJ3YXJuaW5nXCIsIFwiZXJyb3JcIl0uaW5kZXhPZihsb2dMZXZlbCkgPj0gMCAmJiBsZXZlbCA9PT0gXCJlcnJvclwiKVxyXG5cdFx0cmV0dXJuIGNvbnNvbGUuZXJyb3IobXNnKTtcclxufVxyXG5cclxuLy8gU2VuZCBtZXNzYWdlcyB0byB0aGUgb3V0c2lkZSwgc28gcGx1Z2lucyBjYW4gY29uc3VtZSBpdC5cclxuZnVuY3Rpb24gc2VuZE1zZyh0eXBlLCBkYXRhKSB7XHJcblx0aWYodHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgJiYgc2VsZi53aW5kb3cpIHtcclxuXHRcdHNlbGYucG9zdE1lc3NhZ2Uoe1xyXG5cdFx0XHR0eXBlOiBcIndlYnBhY2tcIiArIHR5cGUsXHJcblx0XHRcdGRhdGE6IGRhdGFcclxuXHRcdH0sIFwiKlwiKTtcclxuXHR9XHJcbn1cclxuXHJcbnZhciBvblNvY2tldE1zZyA9IHtcclxuXHRob3Q6IGZ1bmN0aW9uKCkge1xyXG5cdFx0aG90ID0gdHJ1ZTtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBIb3QgTW9kdWxlIFJlcGxhY2VtZW50IGVuYWJsZWQuXCIpO1xyXG5cdH0sXHJcblx0aW52YWxpZDogZnVuY3Rpb24oKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQXBwIHVwZGF0ZWQuIFJlY29tcGlsaW5nLi4uXCIpO1xyXG5cdFx0c2VuZE1zZyhcIkludmFsaWRcIik7XHJcblx0fSxcclxuXHRoYXNoOiBmdW5jdGlvbihoYXNoKSB7XHJcblx0XHRjdXJyZW50SGFzaCA9IGhhc2g7XHJcblx0fSxcclxuXHRcInN0aWxsLW9rXCI6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIE5vdGhpbmcgY2hhbmdlZC5cIilcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5IHx8IHVzZUVycm9yT3ZlcmxheSkgb3ZlcmxheS5jbGVhcigpO1xyXG5cdFx0c2VuZE1zZyhcIlN0aWxsT2tcIik7XHJcblx0fSxcclxuXHRcImxvZy1sZXZlbFwiOiBmdW5jdGlvbihsZXZlbCkge1xyXG5cdFx0bG9nTGV2ZWwgPSBsZXZlbDtcclxuXHR9LFxyXG5cdFwib3ZlcmxheVwiOiBmdW5jdGlvbihvdmVybGF5KSB7XHJcblx0XHRpZih0eXBlb2YgZG9jdW1lbnQgIT09IFwidW5kZWZpbmVkXCIpIHtcclxuXHRcdFx0aWYodHlwZW9mKG92ZXJsYXkpID09PSBcImJvb2xlYW5cIikge1xyXG5cdFx0XHRcdHVzZVdhcm5pbmdPdmVybGF5ID0gb3ZlcmxheTtcclxuXHRcdFx0XHR1c2VFcnJvck92ZXJsYXkgPSBvdmVybGF5O1xyXG5cdFx0XHR9IGVsc2UgaWYob3ZlcmxheSkge1xyXG5cdFx0XHRcdHVzZVdhcm5pbmdPdmVybGF5ID0gb3ZlcmxheS53YXJuaW5ncztcclxuXHRcdFx0XHR1c2VFcnJvck92ZXJsYXkgPSBvdmVybGF5LmVycm9ycztcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0sXHJcblx0b2s6IGZ1bmN0aW9uKCkge1xyXG5cdFx0c2VuZE1zZyhcIk9rXCIpO1xyXG5cdFx0aWYodXNlV2FybmluZ092ZXJsYXkgfHwgdXNlRXJyb3JPdmVybGF5KSBvdmVybGF5LmNsZWFyKCk7XHJcblx0XHRpZihpbml0aWFsKSByZXR1cm4gaW5pdGlhbCA9IGZhbHNlO1xyXG5cdFx0cmVsb2FkQXBwKCk7XHJcblx0fSxcclxuXHRcImNvbnRlbnQtY2hhbmdlZFwiOiBmdW5jdGlvbigpIHtcclxuXHRcdGxvZyhcImluZm9cIiwgXCJbV0RTXSBDb250ZW50IGJhc2UgY2hhbmdlZC4gUmVsb2FkaW5nLi4uXCIpXHJcblx0XHRzZWxmLmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdH0sXHJcblx0d2FybmluZ3M6IGZ1bmN0aW9uKHdhcm5pbmdzKSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gV2FybmluZ3Mgd2hpbGUgY29tcGlsaW5nLlwiKTtcclxuXHRcdHZhciBzdHJpcHBlZFdhcm5pbmdzID0gd2FybmluZ3MubWFwKGZ1bmN0aW9uKHdhcm5pbmcpIHtcclxuXHRcdFx0cmV0dXJuIHN0cmlwQW5zaSh3YXJuaW5nKTtcclxuXHRcdH0pO1xyXG5cdFx0c2VuZE1zZyhcIldhcm5pbmdzXCIsIHN0cmlwcGVkV2FybmluZ3MpO1xyXG5cdFx0Zm9yKHZhciBpID0gMDsgaSA8IHN0cmlwcGVkV2FybmluZ3MubGVuZ3RoOyBpKyspXHJcblx0XHRcdGNvbnNvbGUud2FybihzdHJpcHBlZFdhcm5pbmdzW2ldKTtcclxuXHRcdGlmKHVzZVdhcm5pbmdPdmVybGF5KSBvdmVybGF5LnNob3dNZXNzYWdlKHdhcm5pbmdzKTtcclxuXHJcblx0XHRpZihpbml0aWFsKSByZXR1cm4gaW5pdGlhbCA9IGZhbHNlO1xyXG5cdFx0cmVsb2FkQXBwKCk7XHJcblx0fSxcclxuXHRlcnJvcnM6IGZ1bmN0aW9uKGVycm9ycykge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEVycm9ycyB3aGlsZSBjb21waWxpbmcuIFJlbG9hZCBwcmV2ZW50ZWQuXCIpO1xyXG5cdFx0dmFyIHN0cmlwcGVkRXJyb3JzID0gZXJyb3JzLm1hcChmdW5jdGlvbihlcnJvcikge1xyXG5cdFx0XHRyZXR1cm4gc3RyaXBBbnNpKGVycm9yKTtcclxuXHRcdH0pO1xyXG5cdFx0c2VuZE1zZyhcIkVycm9yc1wiLCBzdHJpcHBlZEVycm9ycyk7XHJcblx0XHRmb3IodmFyIGkgPSAwOyBpIDwgc3RyaXBwZWRFcnJvcnMubGVuZ3RoOyBpKyspXHJcblx0XHRcdGNvbnNvbGUuZXJyb3Ioc3RyaXBwZWRFcnJvcnNbaV0pO1xyXG5cdFx0aWYodXNlRXJyb3JPdmVybGF5KSBvdmVybGF5LnNob3dNZXNzYWdlKGVycm9ycyk7XHJcblx0fSxcclxuXHRlcnJvcjogZnVuY3Rpb24oZXJyb3IpIHtcclxuXHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xyXG5cdH0sXHJcblx0Y2xvc2U6IGZ1bmN0aW9uKCkge1xyXG5cdFx0bG9nKFwiZXJyb3JcIiwgXCJbV0RTXSBEaXNjb25uZWN0ZWQhXCIpO1xyXG5cdFx0c2VuZE1zZyhcIkNsb3NlXCIpO1xyXG5cdH1cclxufTtcclxuXHJcbnZhciBob3N0bmFtZSA9IHVybFBhcnRzLmhvc3RuYW1lO1xyXG52YXIgcHJvdG9jb2wgPSB1cmxQYXJ0cy5wcm90b2NvbDtcclxuXHJcblxyXG4vL2NoZWNrIGlwdjQgYW5kIGlwdjYgYGFsbCBob3N0bmFtZWBcclxuaWYoaG9zdG5hbWUgPT09IFwiMC4wLjAuMFwiIHx8IGhvc3RuYW1lID09PSBcIjo6XCIpIHtcclxuXHQvLyB3aHkgZG8gd2UgbmVlZCB0aGlzIGNoZWNrP1xyXG5cdC8vIGhvc3RuYW1lIG4vYSBmb3IgZmlsZSBwcm90b2NvbCAoZXhhbXBsZSwgd2hlbiB1c2luZyBlbGVjdHJvbiwgaW9uaWMpXHJcblx0Ly8gc2VlOiBodHRwczovL2dpdGh1Yi5jb20vd2VicGFjay93ZWJwYWNrLWRldi1zZXJ2ZXIvcHVsbC8zODRcclxuXHRpZihzZWxmLmxvY2F0aW9uLmhvc3RuYW1lICYmICEhfnNlbGYubG9jYXRpb24ucHJvdG9jb2wuaW5kZXhPZihcImh0dHBcIikpIHtcclxuXHRcdGhvc3RuYW1lID0gc2VsZi5sb2NhdGlvbi5ob3N0bmFtZTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIGBob3N0bmFtZWAgY2FuIGJlIGVtcHR5IHdoZW4gdGhlIHNjcmlwdCBwYXRoIGlzIHJlbGF0aXZlLiBJbiB0aGF0IGNhc2UsIHNwZWNpZnlpbmdcclxuLy8gYSBwcm90b2NvbCB3b3VsZCByZXN1bHQgaW4gYW4gaW52YWxpZCBVUkwuXHJcbi8vIFdoZW4gaHR0cHMgaXMgdXNlZCBpbiB0aGUgYXBwLCBzZWN1cmUgd2Vic29ja2V0cyBhcmUgYWx3YXlzIG5lY2Vzc2FyeVxyXG4vLyBiZWNhdXNlIHRoZSBicm93c2VyIGRvZXNuJ3QgYWNjZXB0IG5vbi1zZWN1cmUgd2Vic29ja2V0cy5cclxuaWYoaG9zdG5hbWUgJiYgKHNlbGYubG9jYXRpb24ucHJvdG9jb2wgPT09IFwiaHR0cHM6XCIgfHwgdXJsUGFydHMuaG9zdG5hbWUgPT09IFwiMC4wLjAuMFwiKSkge1xyXG5cdHByb3RvY29sID0gc2VsZi5sb2NhdGlvbi5wcm90b2NvbDtcclxufVxyXG5cclxudmFyIHNvY2tldFVybCA9IHVybC5mb3JtYXQoe1xyXG5cdHByb3RvY29sOiBwcm90b2NvbCxcclxuXHRhdXRoOiB1cmxQYXJ0cy5hdXRoLFxyXG5cdGhvc3RuYW1lOiBob3N0bmFtZSxcclxuXHRwb3J0OiAodXJsUGFydHMucG9ydCA9PT0gXCIwXCIpID8gc2VsZi5sb2NhdGlvbi5wb3J0IDogdXJsUGFydHMucG9ydCxcclxuXHRwYXRobmFtZTogdXJsUGFydHMucGF0aCA9PSBudWxsIHx8IHVybFBhcnRzLnBhdGggPT09IFwiL1wiID8gXCIvc29ja2pzLW5vZGVcIiA6IHVybFBhcnRzLnBhdGhcclxufSk7XHJcblxyXG5zb2NrZXQoc29ja2V0VXJsLCBvblNvY2tldE1zZyk7XHJcblxyXG52YXIgaXNVbmxvYWRpbmcgPSBmYWxzZTtcclxuc2VsZi5hZGRFdmVudExpc3RlbmVyKFwiYmVmb3JldW5sb2FkXCIsIGZ1bmN0aW9uKCkge1xyXG5cdGlzVW5sb2FkaW5nID0gdHJ1ZTtcclxufSk7XHJcblxyXG5mdW5jdGlvbiByZWxvYWRBcHAoKSB7XHJcblx0aWYoaXNVbmxvYWRpbmcpIHtcclxuXHRcdHJldHVybjtcclxuXHR9XHJcblx0aWYoaG90KSB7XHJcblx0XHRsb2coXCJpbmZvXCIsIFwiW1dEU10gQXBwIGhvdCB1cGRhdGUuLi5cIik7XHJcblx0XHR2YXIgaG90RW1pdHRlciA9IHJlcXVpcmUoXCJ3ZWJwYWNrL2hvdC9lbWl0dGVyXCIpO1xyXG5cdFx0aG90RW1pdHRlci5lbWl0KFwid2VicGFja0hvdFVwZGF0ZVwiLCBjdXJyZW50SGFzaCk7XHJcblx0XHRpZih0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiAmJiBzZWxmLndpbmRvdykge1xyXG5cdFx0XHQvLyBicm9hZGNhc3QgdXBkYXRlIHRvIHdpbmRvd1xyXG5cdFx0XHRzZWxmLnBvc3RNZXNzYWdlKFwid2VicGFja0hvdFVwZGF0ZVwiICsgY3VycmVudEhhc2gsIFwiKlwiKTtcclxuXHRcdH1cclxuXHR9IGVsc2Uge1xyXG5cdFx0bG9nKFwiaW5mb1wiLCBcIltXRFNdIEFwcCB1cGRhdGVkLiBSZWxvYWRpbmcuLi5cIik7XHJcblx0XHRzZWxmLmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdH1cclxufVxyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAod2VicGFjayktZGV2LXNlcnZlci9jbGllbnQ/aHR0cDovL2xvY2FsaG9zdDo4MDgxL1xuLy8gbW9kdWxlIGlkID0gNzc2XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgRHJhd2VyIGZyb20gJ21hdGVyaWFsLXVpL0RyYXdlcic7XG5pbXBvcnQgTWVudUl0ZW0gZnJvbSAnbWF0ZXJpYWwtdWkvTWVudUl0ZW0nO1xuaW1wb3J0IExpc3RJY29uIGZyb20gJ3JlYWN0LW1hdGVyaWFsLWljb25zL2ljb25zL2FjdGlvbi92aWV3LWxpc3QnO1xuaW1wb3J0IEFkZEljb24gZnJvbSAncmVhY3QtbWF0ZXJpYWwtaWNvbnMvaWNvbnMvYWN0aW9uL25vdGUtYWRkJztcbmltcG9ydCBTZWFyY2hJY29uIGZyb20gJ3JlYWN0LW1hdGVyaWFsLWljb25zL2ljb25zL2FjdGlvbi9zZWFyY2gnO1xuaW1wb3J0IEZhdm9yaXRlSWNvbiBmcm9tICdyZWFjdC1tYXRlcmlhbC1pY29ucy9pY29ucy9hY3Rpb24vZmF2b3JpdGUnO1xuaW1wb3J0IHN0eWxlIGZyb20gJy4uL2Fzc2V0cy9zdHlsZU9iaic7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFzaWRlTWVudUNvbXBvbmVudCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG5cdHJlbmRlcigpIHtcblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtYWluLXdyYXBwZXJcIj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJhc2lkZS13cmFwcGVyXCI+XG5cdFx0XHRcdFx0PERyYXdlciBvcGVuPXt0cnVlfSB3aWR0aD17NzV9PlxuXHRcdFx0XHRcdFx0PE1lbnVJdGVtIHN0eWxlPXtzdHlsZS5saXN0SXRlbX0+PExpc3RJY29uLz48L01lbnVJdGVtPlxuXHRcdFx0XHRcdFx0PE1lbnVJdGVtIHN0eWxlPXtzdHlsZS5saXN0SXRlbX0+PEFkZEljb24vPjwvTWVudUl0ZW0+XG5cdFx0XHRcdFx0XHQ8TWVudUl0ZW0gc3R5bGU9e3N0eWxlLmxpc3RJdGVtfT48U2VhcmNoSWNvbi8+PC9NZW51SXRlbT5cblx0XHRcdFx0XHRcdDxNZW51SXRlbSBzdHlsZT17c3R5bGUubGlzdEl0ZW19PjxGYXZvcml0ZUljb24vPjwvTWVudUl0ZW0+XG5cdFx0XHRcdFx0PC9EcmF3ZXI+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcblx0fVxufVxuXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY29tcG9uZW50cy9Bc2lkZU1lbnVDb21wb25lbnQuanMiLCJleHBvcnQgQXNpZGVNZW51Q29tcG9uZW50IGZyb20gJy4vQXNpZGVNZW51Q29tcG9uZW50JztcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY29tcG9uZW50cy9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=