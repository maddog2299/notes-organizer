import React from 'react';
import { AsideMenuComponent } from '../components';

export default class AppContainer extends React.Component {
  render() {
    return (
	    <AsideMenuComponent />
    );
  }
}

