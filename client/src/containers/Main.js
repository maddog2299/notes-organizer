import React from 'react';
import '../assets/styles/main.scss';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
injectTapEventPlugin();

class Main extends React.Component {
	render() {
		return (
			<MuiThemeProvider>
				<section className="content-section">
					{this.props.children}
				</section>
			</MuiThemeProvider>
		);
	}
}

export default Main;
