import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import ListIcon from 'react-material-icons/icons/action/view-list';
import AddIcon from 'react-material-icons/icons/action/note-add';
import SearchIcon from 'react-material-icons/icons/action/search';
import FavoriteIcon from 'react-material-icons/icons/action/favorite';

export default class AsideMenuComponent extends React.Component {
	render() {
		return (
			<div className="aside-wrapper">
				<Drawer containerClassName = "aside-wrapper--overlay" open={true} width={75}>
					<MenuItem><ListIcon /></MenuItem>
					<MenuItem><AddIcon /></MenuItem>
					<MenuItem><SearchIcon /></MenuItem>
					<MenuItem><FavoriteIcon /></MenuItem>
				</Drawer>
			</div>
		);
	}
}

