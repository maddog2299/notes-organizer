import React from 'react';
import ReactDOM from 'react-dom';
import { ReduxAsyncConnect } from 'redux-async-connect';
import { hashHistory, Router } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import store from './redux/store';
import getRoutes from './routes';

const history = syncHistoryWithStore(hashHistory, store);
const div = document.createElement('div');

document.body.appendChild(div);

const component = (
  <Router render={(props) => <ReduxAsyncConnect {...props}/>} history={history}>
    {getRoutes(store)}
  </Router>
);
ReactDOM.render(
    <Provider store={store} key="provider">
	    {component}
    </Provider>,
  div
);
