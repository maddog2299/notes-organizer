
import { combineReducers } from 'redux';
import { reducer as reduxAsyncConnect } from 'redux-async-connect';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
	reduxAsyncConnect,
	routing: routerReducer
});
