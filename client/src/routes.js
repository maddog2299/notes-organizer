import React from 'react';
import { Route } from 'react-router';
import Main from './containers/Main';
import App from './containers/AppContainer';

export default store => {
    return (
      <Route name="home" component={Main}>
        <Route path="/" component={App} />
      </Route>
    );
}
